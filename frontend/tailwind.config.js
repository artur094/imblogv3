const colors = require('tailwindcss/colors')

module.exports = {
  mode: "jit",
  theme: {
    colors: {
      primary: colors.blue,
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      red: colors.red,
    },
    fontFamily: {
      sans: ["ui-sans-serif", "system-ui"],
      serif: ["ui-serif", "Georgia"],
      mono: ["ui-monospace", "SFMono-Regular"],
    },
  },
  purge: {
    enabled: true,
    content: ["./src/**/*.{html,ts}"],
  },
};
