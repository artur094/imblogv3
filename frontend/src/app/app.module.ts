import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  AccountResourceService,
  UserJwtControllerService,
} from '@core/http-gen/services';
import { environment } from '../environments/environment';
import { ApiModule } from '@core/http-gen/api.module';
import { HttpClientModule } from '@angular/common/http';
import {
  AuthenticationProvider,
  LocalStorageProvider,
  NgxAuthUtilsModule,
  StorageProvider,
} from 'ngx-auth-utils';
import { BackendAuthenticationProvider } from '@core/providers/backend-authentication.provider';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { DefaultIfNullPipe } from './shared/pipes/default-if-null.pipe';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        ApiModule.forRoot({rootUrl: environment.apiUrl}),
        NgxAuthUtilsModule.forRoot({
            authenticationProvider: {
                provide: AuthenticationProvider,
                useClass: BackendAuthenticationProvider,
                deps: [AccountResourceService, UserJwtControllerService],
            },
            storageProvider: {
                provide: StorageProvider,
                useClass: LocalStorageProvider,
            },
            homeUrl: '/',
            noAuthRedirectUrl: '/user/login',
            sessionExpiredRedirectUrl: '/user/login',
            refreshToken: false,
            storageKeyPrefix: 'imblog',
        }),
        StoreModule.forRoot({}, {}),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
        EffectsModule.forRoot([]),
    ],
    providers: [],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule {}
