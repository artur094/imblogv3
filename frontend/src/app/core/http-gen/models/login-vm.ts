/* tslint:disable */
/* eslint-disable */
export interface LoginVm {
  password: string;
  rememberMe?: boolean;
  username: string;
}
