/* tslint:disable */
/* eslint-disable */
export interface UserArticleReactionDto {
  reaction: 'DISLIKE' | 'LIKE' | 'LOVE';
}
