/* tslint:disable */
/* eslint-disable */
export interface UserArticleReactionKey {
  articleId?: number;
  userId?: number;
}
