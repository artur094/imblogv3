/* tslint:disable */
/* eslint-disable */
import { Tag } from './tag';
import { User } from './user';
import { UserArticleReaction } from './user-article-reaction';
export interface Article {
  body?: string;
  created: string;
  id?: number;
  owner?: User;
  reactions?: Array<UserArticleReaction>;
  status: 'DELETED' | 'DRAFT' | 'PUBLISHED';
  tags?: Array<Tag>;
  title: string;
}
