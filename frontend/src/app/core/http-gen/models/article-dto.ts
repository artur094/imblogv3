/* tslint:disable */
/* eslint-disable */
import { TagDto } from './tag-dto';
import { UserDto } from './user-dto';
export interface ArticleDto {
  body?: string;
  created?: string;
  id?: number;
  owner?: UserDto;
  status: 'DELETED' | 'DRAFT' | 'PUBLISHED';
  tags?: Array<TagDto>;
  title: string;
}
