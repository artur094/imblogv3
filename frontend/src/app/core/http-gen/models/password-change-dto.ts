/* tslint:disable */
/* eslint-disable */
export interface PasswordChangeDto {
  currentPassword?: string;
  newPassword?: string;
}
