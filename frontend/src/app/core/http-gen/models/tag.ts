/* tslint:disable */
/* eslint-disable */
import { Article } from './article';
export interface Tag {
  articles?: Array<Article>;
  id?: number;
  name: string;
}
