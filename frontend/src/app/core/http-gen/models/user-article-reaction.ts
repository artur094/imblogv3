/* tslint:disable */
/* eslint-disable */
import { Article } from './article';
import { User } from './user';
import { UserArticleReactionKey } from './user-article-reaction-key';
export interface UserArticleReaction {
  article?: Article;
  id?: UserArticleReactionKey;
  reaction: 'DISLIKE' | 'LIKE' | 'LOVE';
  user?: User;
}
