/* tslint:disable */
/* eslint-disable */
import { Article } from './article';
import { UserArticleReaction } from './user-article-reaction';
export interface User {
  accountNonExpired?: boolean;
  accountNonLocked?: boolean;
  activated: boolean;
  admin?: boolean;
  credentialsNonExpired?: boolean;
  editor?: boolean;
  email?: string;
  enabled?: boolean;
  firstName?: string;
  id?: number;
  imageUrl?: string;
  langKey?: string;
  lastName?: string;
  login: string;
  ownedArticles?: Array<Article>;
  reactions?: Array<UserArticleReaction>;
  resetDate?: string;
  username?: string;
}
