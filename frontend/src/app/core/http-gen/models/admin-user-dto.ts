/* tslint:disable */
/* eslint-disable */
export interface AdminUserDto {
  activated?: boolean;
  authorities?: Array<string>;
  createdBy?: string;
  createdDate?: string;
  email?: string;
  firstName?: string;
  id?: number;
  imageUrl?: string;
  langKey?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  lastName?: string;
  login: string;
}
