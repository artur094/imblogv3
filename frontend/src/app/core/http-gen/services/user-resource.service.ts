/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AdminUserDto } from '../models/admin-user-dto';
import { User } from '../models/user';
import { UserDto } from '../models/user-dto';

/**
 * User Resource
 */
@Injectable({
  providedIn: 'root',
})
export class UserResourceService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation getAllUsersUsingGet
   */
  static readonly GetAllUsersUsingGetPath = '/api/auth/users';

  /**
   * getAllUsers.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllUsersUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUsersUsingGet$Response(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<StrictHttpResponse<Array<AdminUserDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.GetAllUsersUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('page', params.page, { style: 'form' });
      rb.query('size', params.size, { style: 'form' });
      rb.query('sort', params.sort, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<AdminUserDto>>;
        })
      );
  }

  /**
   * getAllUsers.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllUsersUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUsersUsingGet(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<Array<AdminUserDto>> {
    return this.getAllUsersUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<Array<AdminUserDto>>) =>
          r.body as Array<AdminUserDto>
      )
    );
  }

  /**
   * Path part for operation updateUserUsingPut
   */
  static readonly UpdateUserUsingPutPath = '/api/auth/users';

  /**
   * updateUser.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateUserUsingPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUserUsingPut$Response(params?: {
    body?: AdminUserDto;
  }): Observable<StrictHttpResponse<AdminUserDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.UpdateUserUsingPutPath,
      'put'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<AdminUserDto>;
        })
      );
  }

  /**
   * updateUser.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateUserUsingPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateUserUsingPut(params?: {
    body?: AdminUserDto;
  }): Observable<AdminUserDto> {
    return this.updateUserUsingPut$Response(params).pipe(
      map((r: StrictHttpResponse<AdminUserDto>) => r.body as AdminUserDto)
    );
  }

  /**
   * Path part for operation createUserUsingPost
   */
  static readonly CreateUserUsingPostPath = '/api/auth/users';

  /**
   * createUser.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUserUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUserUsingPost$Response(params?: {
    body?: AdminUserDto;
  }): Observable<StrictHttpResponse<User>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.CreateUserUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<User>;
        })
      );
  }

  /**
   * createUser.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createUserUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUserUsingPost(params?: { body?: AdminUserDto }): Observable<User> {
    return this.createUserUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<User>) => r.body as User)
    );
  }

  /**
   * Path part for operation getIdentityUsingGet
   */
  static readonly GetIdentityUsingGetPath = '/api/auth/users/me';

  /**
   * getIdentity.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getIdentityUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIdentityUsingGet$Response(params?: {}): Observable<
    StrictHttpResponse<UserDto>
  > {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.GetIdentityUsingGetPath,
      'get'
    );
    if (params) {
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<UserDto>;
        })
      );
  }

  /**
   * getIdentity.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getIdentityUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIdentityUsingGet(params?: {}): Observable<UserDto> {
    return this.getIdentityUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<UserDto>) => r.body as UserDto)
    );
  }

  /**
   * Path part for operation getUserUsingGet
   */
  static readonly GetUserUsingGetPath = '/api/auth/users/{login}';

  /**
   * getUser.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUserUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserUsingGet$Response(params: {
    /**
     * login
     */
    login: string;
  }): Observable<StrictHttpResponse<AdminUserDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.GetUserUsingGetPath,
      'get'
    );
    if (params) {
      rb.path('login', params.login, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<AdminUserDto>;
        })
      );
  }

  /**
   * getUser.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getUserUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUserUsingGet(params: {
    /**
     * login
     */
    login: string;
  }): Observable<AdminUserDto> {
    return this.getUserUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<AdminUserDto>) => r.body as AdminUserDto)
    );
  }

  /**
   * Path part for operation deleteUserUsingDelete
   */
  static readonly DeleteUserUsingDeletePath = '/api/auth/users/{login}';

  /**
   * deleteUser.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteUserUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserUsingDelete$Response(params: {
    /**
     * login
     */
    login: string;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserResourceService.DeleteUserUsingDeletePath,
      'delete'
    );
    if (params) {
      rb.path('login', params.login, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * deleteUser.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteUserUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteUserUsingDelete(params: {
    /**
     * login
     */
    login: string;
  }): Observable<void> {
    return this.deleteUserUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }
}
