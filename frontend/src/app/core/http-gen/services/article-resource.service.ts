/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ArticleDto } from '../models/article-dto';
import { TagDto } from '../models/tag-dto';
import { UserArticleReactionDto } from '../models/user-article-reaction-dto';

/**
 * Article Resource
 */
@Injectable({
  providedIn: 'root',
})
export class ArticleResourceService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation createArticleUsingPost
   */
  static readonly CreateArticleUsingPostPath = '/api/articles';

  /**
   * createArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createArticleUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createArticleUsingPost$Response(params?: {
    body?: ArticleDto;
  }): Observable<StrictHttpResponse<ArticleDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.CreateArticleUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<ArticleDto>;
        })
      );
  }

  /**
   * createArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createArticleUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createArticleUsingPost(params?: {
    body?: ArticleDto;
  }): Observable<ArticleDto> {
    return this.createArticleUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<ArticleDto>) => r.body as ArticleDto)
    );
  }

  /**
   * Path part for operation getAllDeletedArticlesUsingGet
   */
  static readonly GetAllDeletedArticlesUsingGetPath = '/api/articles/deleted';

  /**
   * getAllDeletedArticles.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllDeletedArticlesUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDeletedArticlesUsingGet$Response(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<StrictHttpResponse<Array<ArticleDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetAllDeletedArticlesUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('page', params.page, { style: 'form' });
      rb.query('size', params.size, { style: 'form' });
      rb.query('sort', params.sort, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<ArticleDto>>;
        })
      );
  }

  /**
   * getAllDeletedArticles.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllDeletedArticlesUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDeletedArticlesUsingGet(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<Array<ArticleDto>> {
    return this.getAllDeletedArticlesUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<Array<ArticleDto>>) =>
          r.body as Array<ArticleDto>
      )
    );
  }

  /**
   * Path part for operation getAllDraftArticlesUsingGet
   */
  static readonly GetAllDraftArticlesUsingGetPath = '/api/articles/draft';

  /**
   * getAllDraftArticles.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllDraftArticlesUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDraftArticlesUsingGet$Response(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<StrictHttpResponse<Array<ArticleDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetAllDraftArticlesUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('page', params.page, { style: 'form' });
      rb.query('size', params.size, { style: 'form' });
      rb.query('sort', params.sort, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<ArticleDto>>;
        })
      );
  }

  /**
   * getAllDraftArticles.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllDraftArticlesUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllDraftArticlesUsingGet(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<Array<ArticleDto>> {
    return this.getAllDraftArticlesUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<Array<ArticleDto>>) =>
          r.body as Array<ArticleDto>
      )
    );
  }

  /**
   * Path part for operation getAllPublishedArticlesUsingGet
   */
  static readonly GetAllPublishedArticlesUsingGetPath =
    '/api/articles/published';

  /**
   * getAllPublishedArticles.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllPublishedArticlesUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllPublishedArticlesUsingGet$Response(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<StrictHttpResponse<Array<ArticleDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetAllPublishedArticlesUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('page', params.page, { style: 'form' });
      rb.query('size', params.size, { style: 'form' });
      rb.query('sort', params.sort, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<ArticleDto>>;
        })
      );
  }

  /**
   * getAllPublishedArticles.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllPublishedArticlesUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllPublishedArticlesUsingGet(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<Array<ArticleDto>> {
    return this.getAllPublishedArticlesUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<Array<ArticleDto>>) =>
          r.body as Array<ArticleDto>
      )
    );
  }

  /**
   * Path part for operation getArticleUsingGet
   */
  static readonly GetArticleUsingGetPath = '/api/articles/{id}';

  /**
   * getArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getArticleUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getArticleUsingGet$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<ArticleDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetArticleUsingGetPath,
      'get'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<ArticleDto>;
        })
      );
  }

  /**
   * getArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getArticleUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getArticleUsingGet(params: {
    /**
     * id
     */
    id: number;
  }): Observable<ArticleDto> {
    return this.getArticleUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<ArticleDto>) => r.body as ArticleDto)
    );
  }

  /**
   * Path part for operation updateArticleUsingPut
   */
  static readonly UpdateArticleUsingPutPath = '/api/articles/{id}';

  /**
   * updateArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateArticleUsingPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateArticleUsingPut$Response(params: {
    /**
     * id
     */
    id: number;
    body?: ArticleDto;
  }): Observable<StrictHttpResponse<ArticleDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.UpdateArticleUsingPutPath,
      'put'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<ArticleDto>;
        })
      );
  }

  /**
   * updateArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateArticleUsingPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateArticleUsingPut(params: {
    /**
     * id
     */
    id: number;
    body?: ArticleDto;
  }): Observable<ArticleDto> {
    return this.updateArticleUsingPut$Response(params).pipe(
      map((r: StrictHttpResponse<ArticleDto>) => r.body as ArticleDto)
    );
  }

  /**
   * Path part for operation deleteArticleUsingDelete
   */
  static readonly DeleteArticleUsingDeletePath = '/api/articles/{id}';

  /**
   * deleteArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteArticleUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteArticleUsingDelete$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<{}>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.DeleteArticleUsingDeletePath,
      'delete'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<{}>;
        })
      );
  }

  /**
   * deleteArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteArticleUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteArticleUsingDelete(params: {
    /**
     * id
     */
    id: number;
  }): Observable<{}> {
    return this.deleteArticleUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<{}>) => r.body as {})
    );
  }

  /**
   * Path part for operation partialUpdateArticleUsingPatch
   */
  static readonly PartialUpdateArticleUsingPatchPath = '/api/articles/{id}';

  /**
   * partialUpdateArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `partialUpdateArticleUsingPatch()` instead.
   *
   * This method sends `application/merge-patch+json` and handles request body of type `application/merge-patch+json`.
   */
  partialUpdateArticleUsingPatch$Response(params: {
    /**
     * id
     */
    id: number;
    body?: ArticleDto;
  }): Observable<StrictHttpResponse<ArticleDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.PartialUpdateArticleUsingPatchPath,
      'patch'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
      rb.body(params.body, 'application/merge-patch+json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<ArticleDto>;
        })
      );
  }

  /**
   * partialUpdateArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `partialUpdateArticleUsingPatch$Response()` instead.
   *
   * This method sends `application/merge-patch+json` and handles request body of type `application/merge-patch+json`.
   */
  partialUpdateArticleUsingPatch(params: {
    /**
     * id
     */
    id: number;
    body?: ArticleDto;
  }): Observable<ArticleDto> {
    return this.partialUpdateArticleUsingPatch$Response(params).pipe(
      map((r: StrictHttpResponse<ArticleDto>) => r.body as ArticleDto)
    );
  }

  /**
   * Path part for operation getAllReactionsOfArticleUsingGet
   */
  static readonly GetAllReactionsOfArticleUsingGetPath =
    '/api/articles/{id}/reactions';

  /**
   * getAllReactionsOfArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllReactionsOfArticleUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllReactionsOfArticleUsingGet$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<Array<UserArticleReactionDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetAllReactionsOfArticleUsingGetPath,
      'get'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<UserArticleReactionDto>>;
        })
      );
  }

  /**
   * getAllReactionsOfArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllReactionsOfArticleUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllReactionsOfArticleUsingGet(params: {
    /**
     * id
     */
    id: number;
  }): Observable<Array<UserArticleReactionDto>> {
    return this.getAllReactionsOfArticleUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<Array<UserArticleReactionDto>>) =>
          r.body as Array<UserArticleReactionDto>
      )
    );
  }

  /**
   * Path part for operation addOrUpdateReactionToArticleUsingPost
   */
  static readonly AddOrUpdateReactionToArticleUsingPostPath =
    '/api/articles/{id}/reactions';

  /**
   * addOrUpdateReactionToArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `addOrUpdateReactionToArticleUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  addOrUpdateReactionToArticleUsingPost$Response(params: {
    /**
     * id
     */
    id: number;
    body?: UserArticleReactionDto;
  }): Observable<StrictHttpResponse<{}>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.AddOrUpdateReactionToArticleUsingPostPath,
      'post'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<{}>;
        })
      );
  }

  /**
   * addOrUpdateReactionToArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `addOrUpdateReactionToArticleUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  addOrUpdateReactionToArticleUsingPost(params: {
    /**
     * id
     */
    id: number;
    body?: UserArticleReactionDto;
  }): Observable<{}> {
    return this.addOrUpdateReactionToArticleUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<{}>) => r.body as {})
    );
  }

  /**
   * Path part for operation removeReactionUsingDelete
   */
  static readonly RemoveReactionUsingDeletePath =
    '/api/articles/{id}/reactions';

  /**
   * removeReaction.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `removeReactionUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeReactionUsingDelete$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<{}>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.RemoveReactionUsingDeletePath,
      'delete'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<{}>;
        })
      );
  }

  /**
   * removeReaction.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `removeReactionUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeReactionUsingDelete(params: {
    /**
     * id
     */
    id: number;
  }): Observable<{}> {
    return this.removeReactionUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<{}>) => r.body as {})
    );
  }

  /**
   * Path part for operation getReactionUsingGet
   */
  static readonly GetReactionUsingGetPath =
    '/api/articles/{id}/reactions/owner';

  /**
   * getReaction.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getReactionUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getReactionUsingGet$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<UserArticleReactionDto>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetReactionUsingGetPath,
      'get'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<UserArticleReactionDto>;
        })
      );
  }

  /**
   * getReaction.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getReactionUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getReactionUsingGet(params: {
    /**
     * id
     */
    id: number;
  }): Observable<UserArticleReactionDto> {
    return this.getReactionUsingGet$Response(params).pipe(
      map(
        (r: StrictHttpResponse<UserArticleReactionDto>) =>
          r.body as UserArticleReactionDto
      )
    );
  }

  /**
   * Path part for operation getAllArticleTagsUsingGet
   */
  static readonly GetAllArticleTagsUsingGetPath = '/api/articles/{id}/tags';

  /**
   * getAllArticleTags.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllArticleTagsUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllArticleTagsUsingGet$Response(params: {
    /**
     * id
     */
    id: number;
  }): Observable<StrictHttpResponse<Array<TagDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.GetAllArticleTagsUsingGetPath,
      'get'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<TagDto>>;
        })
      );
  }

  /**
   * getAllArticleTags.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllArticleTagsUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllArticleTagsUsingGet(params: {
    /**
     * id
     */
    id: number;
  }): Observable<Array<TagDto>> {
    return this.getAllArticleTagsUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<TagDto>>) => r.body as Array<TagDto>)
    );
  }

  /**
   * Path part for operation addTagToArticleUsingPost
   */
  static readonly AddTagToArticleUsingPostPath = '/api/articles/{id}/tags';

  /**
   * addTagToArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `addTagToArticleUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  addTagToArticleUsingPost$Response(params: {
    /**
     * id
     */
    id: number;
    body?: TagDto;
  }): Observable<StrictHttpResponse<{}>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.AddTagToArticleUsingPostPath,
      'post'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<{}>;
        })
      );
  }

  /**
   * addTagToArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `addTagToArticleUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  addTagToArticleUsingPost(params: {
    /**
     * id
     */
    id: number;
    body?: TagDto;
  }): Observable<{}> {
    return this.addTagToArticleUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<{}>) => r.body as {})
    );
  }

  /**
   * Path part for operation removeTagFromArticleUsingDelete
   */
  static readonly RemoveTagFromArticleUsingDeletePath =
    '/api/articles/{id}/tags/{tag}';

  /**
   * removeTagFromArticle.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `removeTagFromArticleUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeTagFromArticleUsingDelete$Response(params: {
    /**
     * id
     */
    id: number;

    /**
     * tag
     */
    tag: string;
  }): Observable<StrictHttpResponse<{}>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      ArticleResourceService.RemoveTagFromArticleUsingDeletePath,
      'delete'
    );
    if (params) {
      rb.path('id', params.id, { style: 'simple' });
      rb.path('tag', params.tag, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<{}>;
        })
      );
  }

  /**
   * removeTagFromArticle.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `removeTagFromArticleUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  removeTagFromArticleUsingDelete(params: {
    /**
     * id
     */
    id: number;

    /**
     * tag
     */
    tag: string;
  }): Observable<{}> {
    return this.removeTagFromArticleUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<{}>) => r.body as {})
    );
  }
}
