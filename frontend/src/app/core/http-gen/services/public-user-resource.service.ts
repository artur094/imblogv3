/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { UserDto } from '../models/user-dto';

/**
 * Public User Resource
 */
@Injectable({
  providedIn: 'root',
})
export class PublicUserResourceService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation getAuthoritiesUsingGet
   */
  static readonly GetAuthoritiesUsingGetPath = '/api/authorities';

  /**
   * getAuthorities.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAuthoritiesUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAuthoritiesUsingGet$Response(params?: {}): Observable<
    StrictHttpResponse<Array<string>>
  > {
    const rb = new RequestBuilder(
      this.rootUrl,
      PublicUserResourceService.GetAuthoritiesUsingGetPath,
      'get'
    );
    if (params) {
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<string>>;
        })
      );
  }

  /**
   * getAuthorities.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAuthoritiesUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAuthoritiesUsingGet(params?: {}): Observable<Array<string>> {
    return this.getAuthoritiesUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<string>>) => r.body as Array<string>)
    );
  }

  /**
   * Path part for operation getAllPublicUsersUsingGet
   */
  static readonly GetAllPublicUsersUsingGetPath = '/api/users';

  /**
   * getAllPublicUsers.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllPublicUsersUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllPublicUsersUsingGet$Response(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<StrictHttpResponse<Array<UserDto>>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      PublicUserResourceService.GetAllPublicUsersUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('page', params.page, { style: 'form' });
      rb.query('size', params.size, { style: 'form' });
      rb.query('sort', params.sort, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<UserDto>>;
        })
      );
  }

  /**
   * getAllPublicUsers.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllPublicUsersUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllPublicUsersUsingGet(params?: {
    /**
     * Page number of the requested page
     */
    page?: number;

    /**
     * Size of a page
     */
    size?: number;

    /**
     * Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
     */
    sort?: Array<string>;
  }): Observable<Array<UserDto>> {
    return this.getAllPublicUsersUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<UserDto>>) => r.body as Array<UserDto>)
    );
  }
}
