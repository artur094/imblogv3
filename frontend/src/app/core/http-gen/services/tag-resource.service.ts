/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { TagDto } from '../models/tag-dto';

/**
 * Tag Resource
 */
@Injectable({
  providedIn: 'root',
})
export class TagResourceService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation getAllTagsUsingGet
   */
  static readonly GetAllTagsUsingGetPath = '/api/tags';

  /**
   * getAllTags.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllTagsUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTagsUsingGet$Response(params?: {}): Observable<
    StrictHttpResponse<Array<TagDto>>
  > {
    const rb = new RequestBuilder(
      this.rootUrl,
      TagResourceService.GetAllTagsUsingGetPath,
      'get'
    );
    if (params) {
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<Array<TagDto>>;
        })
      );
  }

  /**
   * getAllTags.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllTagsUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTagsUsingGet(params?: {}): Observable<Array<TagDto>> {
    return this.getAllTagsUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<TagDto>>) => r.body as Array<TagDto>)
    );
  }

  /**
   * Path part for operation createTagUsingPost
   */
  static readonly CreateTagUsingPostPath = '/api/tags';

  /**
   * createTag.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createTagUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTagUsingPost$Response(params?: {
    body?: TagDto;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      TagResourceService.CreateTagUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * createTag.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createTagUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTagUsingPost(params?: { body?: TagDto }): Observable<void> {
    return this.createTagUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation deleteTagUsingDelete
   */
  static readonly DeleteTagUsingDeletePath = '/api/tags/{name}';

  /**
   * deleteTag.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteTagUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTagUsingDelete$Response(params: {
    /**
     * name
     */
    name: string;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      TagResourceService.DeleteTagUsingDeletePath,
      'delete'
    );
    if (params) {
      rb.path('name', params.name, { style: 'simple' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * deleteTag.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteTagUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTagUsingDelete(params: {
    /**
     * name
     */
    name: string;
  }): Observable<void> {
    return this.deleteTagUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }
}
