/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AdminUserDto } from '../models/admin-user-dto';
import { KeyAndPasswordVm } from '../models/key-and-password-vm';
import { ManagedUserVm } from '../models/managed-user-vm';
import { PasswordChangeDto } from '../models/password-change-dto';

/**
 * Account Resource
 */
@Injectable({
  providedIn: 'root',
})
export class AccountResourceService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation getAccountUsingGet
   */
  static readonly GetAccountUsingGetPath = '/api/account';

  /**
   * getAccount.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAccountUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAccountUsingGet$Response(params?: {}): Observable<
    StrictHttpResponse<AdminUserDto>
  > {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.GetAccountUsingGetPath,
      'get'
    );
    if (params) {
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<AdminUserDto>;
        })
      );
  }

  /**
   * getAccount.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAccountUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAccountUsingGet(params?: {}): Observable<AdminUserDto> {
    return this.getAccountUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<AdminUserDto>) => r.body as AdminUserDto)
    );
  }

  /**
   * Path part for operation saveAccountUsingPost
   */
  static readonly SaveAccountUsingPostPath = '/api/account';

  /**
   * saveAccount.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `saveAccountUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  saveAccountUsingPost$Response(params?: {
    body?: AdminUserDto;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.SaveAccountUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * saveAccount.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `saveAccountUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  saveAccountUsingPost(params?: { body?: AdminUserDto }): Observable<void> {
    return this.saveAccountUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation changePasswordUsingPost
   */
  static readonly ChangePasswordUsingPostPath = '/api/account/change-password';

  /**
   * changePassword.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `changePasswordUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  changePasswordUsingPost$Response(params?: {
    body?: PasswordChangeDto;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.ChangePasswordUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * changePassword.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `changePasswordUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  changePasswordUsingPost(params?: {
    body?: PasswordChangeDto;
  }): Observable<void> {
    return this.changePasswordUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation finishPasswordResetUsingPost
   */
  static readonly FinishPasswordResetUsingPostPath =
    '/api/account/reset-password/finish';

  /**
   * finishPasswordReset.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `finishPasswordResetUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  finishPasswordResetUsingPost$Response(params?: {
    body?: KeyAndPasswordVm;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.FinishPasswordResetUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * finishPasswordReset.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `finishPasswordResetUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  finishPasswordResetUsingPost(params?: {
    body?: KeyAndPasswordVm;
  }): Observable<void> {
    return this.finishPasswordResetUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation requestPasswordResetUsingPost
   */
  static readonly RequestPasswordResetUsingPostPath =
    '/api/account/reset-password/init';

  /**
   * requestPasswordReset.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `requestPasswordResetUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestPasswordResetUsingPost$Response(params?: {
    body?: string;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.RequestPasswordResetUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * requestPasswordReset.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `requestPasswordResetUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  requestPasswordResetUsingPost(params?: { body?: string }): Observable<void> {
    return this.requestPasswordResetUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation activateAccountUsingGet
   */
  static readonly ActivateAccountUsingGetPath = '/api/activate';

  /**
   * activateAccount.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `activateAccountUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateAccountUsingGet$Response(params: {
    /**
     * key
     */
    key: string;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.ActivateAccountUsingGetPath,
      'get'
    );
    if (params) {
      rb.query('key', params.key, { style: 'form' });
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * activateAccount.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `activateAccountUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  activateAccountUsingGet(params: {
    /**
     * key
     */
    key: string;
  }): Observable<void> {
    return this.activateAccountUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation isAuthenticatedUsingGet
   */
  static readonly IsAuthenticatedUsingGetPath = '/api/authenticate';

  /**
   * isAuthenticated.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `isAuthenticatedUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  isAuthenticatedUsingGet$Response(params?: {}): Observable<
    StrictHttpResponse<string>
  > {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.IsAuthenticatedUsingGetPath,
      'get'
    );
    if (params) {
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<string>;
        })
      );
  }

  /**
   * isAuthenticated.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `isAuthenticatedUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  isAuthenticatedUsingGet(params?: {}): Observable<string> {
    return this.isAuthenticatedUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * Path part for operation registerAccountUsingPost
   */
  static readonly RegisterAccountUsingPostPath = '/api/register';

  /**
   * registerAccount.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `registerAccountUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerAccountUsingPost$Response(params?: {
    body?: ManagedUserVm;
  }): Observable<StrictHttpResponse<void>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      AccountResourceService.RegisterAccountUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'text',
          accept: '*/*',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return (r as HttpResponse<any>).clone({
            body: undefined,
          }) as StrictHttpResponse<void>;
        })
      );
  }

  /**
   * registerAccount.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `registerAccountUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerAccountUsingPost(params?: {
    body?: ManagedUserVm;
  }): Observable<void> {
    return this.registerAccountUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }
}
