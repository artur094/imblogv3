/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { JwtToken } from '../models/jwt-token';
import { LoginVm } from '../models/login-vm';

/**
 * User JWT Controller
 */
@Injectable({
  providedIn: 'root',
})
export class UserJwtControllerService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /**
   * Path part for operation authorizeUsingPost
   */
  static readonly AuthorizeUsingPostPath = '/api/authenticate';

  /**
   * authorize.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `authorizeUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  authorizeUsingPost$Response(params?: {
    body?: LoginVm;
  }): Observable<StrictHttpResponse<JwtToken>> {
    const rb = new RequestBuilder(
      this.rootUrl,
      UserJwtControllerService.AuthorizeUsingPostPath,
      'post'
    );
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http
      .request(
        rb.build({
          responseType: 'json',
          accept: 'application/json',
        })
      )
      .pipe(
        filter((r: any) => r instanceof HttpResponse),
        map((r: HttpResponse<any>) => {
          return r as StrictHttpResponse<JwtToken>;
        })
      );
  }

  /**
   * authorize.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `authorizeUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  authorizeUsingPost(params?: { body?: LoginVm }): Observable<JwtToken> {
    return this.authorizeUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<JwtToken>) => r.body as JwtToken)
    );
  }
}
