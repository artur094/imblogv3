import {AccessTokenModel, AuthenticationProvider} from 'ngx-auth-utils';
import {Observable, tap} from 'rxjs';
import {map} from 'rxjs/operators';
import {AccountResourceService} from '@core/http-gen/services/account-resource.service';
import {UserJwtControllerService} from '@core/http-gen/services/user-jwt-controller.service';
import {LoginVm} from '@core/http-gen/models/login-vm';
import {JwtToken} from '@core/http-gen/models/jwt-token';
import {AdminUserDto} from "@core/http-gen/models/admin-user-dto";

export class BackendAuthenticationProvider extends AuthenticationProvider {
  constructor(
    private accountService: AccountResourceService,
    private tokenService: UserJwtControllerService
  ) {
    super();
  }

  private static mapAccessToken(tokenPair: JwtToken): AccessTokenModel {
    {
      console.log('JWT: ', tokenPair);
      return {
        accessToken: tokenPair.id_token || '',
      };
    }
  }

  fetchUser(): Observable<AdminUserDto> {
    return this.accountService.getAccountUsingGet();
  }

  doLogin(credentials: LoginVm): Observable<AccessTokenModel> {
    return this.tokenService.authorizeUsingPost({ body: credentials }).pipe(
      map(BackendAuthenticationProvider.mapAccessToken),
      tap((data) => console.log('LOGIN: ', data))
    );
  }
}
