import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component } from '@angular/core';
import {BehaviorSubject, delayWhen, interval, Observable, take, timer} from 'rxjs';
import { NgxAuthService } from 'ngx-auth-utils';
import { NavigationService } from '@core/services/navigation.service';
import {AdminUserDto} from "@core/http-gen/models/admin-user-dto";

interface NavbarLinkItem {
  url: string[];
  name: string;
  options?: { exact: boolean };
}

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
  animations: [
    trigger('fadeIn', [
      state('open', style({ opacity: '1' })),
      state('close', style({ opacity: '0'})),
      transition('open <=> close', animate('0.3s ease-in-out')),
      transition(':enter', [
        style({ opacity: '0' }),
        animate('0.3s ease-in-out', style({ opacity: '1' })),
      ]),
    ]),

    trigger('slideIn', [
      state('open', style({ left: '0rem' })),
      state('close', style({ left: '-16rem' })),
      transition('open => close', animate('0.3s ease-in-out')),
      transition(':enter', [
        style({ left: '-16rem' }),
        animate('0.3s ease-in-out', style({ left: '0rem' })),
      ]),
    ]),

    trigger('rotate', [
      state('open', style({ transform: 'rotate(90deg)' })),
      state('close', style({ transform: 'rotate(0deg)' })),
      transition('open <=> close', animate('0.3s ease-in-out')),
    ]),
  ],
})
export class NavigationBarComponent {
  urls: NavbarLinkItem[] = [
    {
      url: ['/'],
      name: 'Home',
      options: { exact: true },
    },
    {
      url: ['/blog'],
      name: 'Blog',
    },
    {
      url: ['/projects'],
      name: 'Projects',
    },
  ];

  userProfileUrls: NavbarLinkItem[] = [
    {
      url: ['/user/profile'],
      name: 'My Profile',
    },
    {
      url: ['/user/settings'],
      name: 'Account Settings',
    },
  ];

  isList?: number;
  isMenu = false;

  showProfileDropdownValue = false;
  showProfileDropdown = new BehaviorSubject(this.showProfileDropdownValue);
  showProfileDropdown$ = this.showProfileDropdown.asObservable();
  showProfileDropdownVisible$ = this.showProfileDropdown$.pipe(
    delayWhen((v) => interval(v ? 0 : 300))
  );

  showMenuValue = false;
  showMenu = new BehaviorSubject(this.showMenuValue);
  showMenu$ = this.showMenu.asObservable();
  showMenuVisible$ = this.showMenu$.pipe(
    delayWhen((v) => interval(v ? 0 : 300))
  );

  user$: Observable<AdminUserDto> = this.authService.state;

  constructor(
    private authService: NgxAuthService,
    private navigationService: NavigationService
  ) {}

  toggleProfileDropdown(): void {
   this.showProfileDropdownValue = !this.showProfileDropdownValue;
   this.showProfileDropdown.next(this.showProfileDropdownValue);
  }

  toggleMenu(): void {
    this.showMenuValue = !this.showMenuValue;
    this.showMenu.next(this.showMenuValue);
  }

  goToLogin(): void {
    this.navigationService.goToLogin();
    this.toggleMenu();
  }

  logout() {
    this.authService.logout();
    this.toggleMenu();
    this.toggleProfileDropdown();
  }
}
