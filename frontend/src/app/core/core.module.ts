import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminNavigationBarComponent} from './components/admin-navigation-bar/admin-navigation-bar.component';
import {NavigationBarComponent} from './components/navigation-bar/navigation-bar.component';
import {FooterComponent} from './components/footer/footer.component';
import {RouterModule} from '@angular/router';
import {RippleModule} from "primeng/ripple";

@NgModule({
  declarations: [
    AdminNavigationBarComponent,
    NavigationBarComponent,
    FooterComponent,
  ],
  imports: [CommonModule, RouterModule, RippleModule],
  exports: [
    AdminNavigationBarComponent,
    NavigationBarComponent,
    FooterComponent,
  ],
})
export class CoreModule {}
