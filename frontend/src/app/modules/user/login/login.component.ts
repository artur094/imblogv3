import {Component} from '@angular/core';
import {NgxAuthService} from 'ngx-auth-utils';
import {FormBuilder, Validators} from '@angular/forms';
import {NavigationService} from '@core/services/navigation.service';
import {catchError, finalize} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginError = false;
  genericError = false;
  loading = false;

  form = this.formBuilder.group({
    username: this.formBuilder.control(undefined, [Validators.required, Validators.minLength(1)]),
    password: this.formBuilder.control(undefined, [Validators.required, Validators.minLength(4)]),
  });

  constructor(
    private authService: NgxAuthService,
    private formBuilder: FormBuilder,
    private navigationService: NavigationService
  ) {}

  formChange(): void {
    this.loginError = false;
    this.genericError = false;
  }

  login(): void {
    this.loading = true;
    this.form.disable();
    this.authService.login(this.form.getRawValue())
      .pipe(
        catchError((error) => {
          if(error.status === 401) {
            this.loginError = true;
          } else {
            this.genericError = true;
          }
          throw new Error(error);
        }),
        finalize(() => {
        this.loading = false;
        this.form.enable();
      }))
      .subscribe((user) => {
      this.navigationService.goToHome();
    });
  }
}
