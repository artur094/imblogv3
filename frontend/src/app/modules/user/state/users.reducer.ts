import {createAction, createReducer, on} from "@ngrx/store";
import {USERS_LIST_UPDATE} from "./users.actions";
import {UserDto} from "@core/http-gen/models/user-dto";
import * as AppState from "@state/app.state"

export interface State extends AppState.State{
  currentUser: UserDto,
}


export const userReducer = createReducer(
  {'articles': []},
  on(createAction(USERS_LIST_UPDATE), state => {
    return {
      ...state
    };
  })
);
