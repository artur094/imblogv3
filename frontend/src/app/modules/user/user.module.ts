import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import {UserRoutingModule} from "@user/user-routing.module";
import { LoginComponent } from './login/login.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RippleModule} from "primeng/ripple";
import {ButtonModule} from "primeng/button";

@NgModule({
  declarations: [ProfileComponent, ProfileSettingsComponent, LoginComponent],
  imports: [CommonModule, UserRoutingModule, CoreModule, HttpClientModule, ReactiveFormsModule, RippleModule, ButtonModule],
})
export class UserModule {}
