import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainProjectsComponent } from './main-projects/main-projects.component';
import { ProjectsRoutingModule } from './projects-routing.module';

@NgModule({
  declarations: [MainProjectsComponent],
  imports: [CommonModule, ProjectsRoutingModule],
})
export class ProjectsModule {}
