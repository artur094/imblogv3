import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainProjectsComponent} from '@projects/main-projects/main-projects.component';

const routes: Routes = [
  {
    path: '',
    component: MainProjectsComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsRoutingModule {}
