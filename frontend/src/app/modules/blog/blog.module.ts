import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainArticlesComponent} from './main-articles/main-articles.component';
import {BlogRoutingModule} from '@blog/blog-routing.module';
import {StoreModule} from "@ngrx/store";
import {articleReducer} from "@blog/state/articles.reducer";
import {EffectsModule} from "@ngrx/effects";
import {ArticlesEffects} from "@blog/state/articles.effects";
import {ArticleSummaryComponent} from './article-summary/article-summary.component';
import {SharedModule} from "@shared/shared.module";
import {RippleModule} from "primeng/ripple";
import { ArticleFormComponent } from './article-form/article-form.component';
import {DynamicDialogModule} from "primeng/dynamicdialog";
import { ArticleEditDialogComponent } from './article-edit-dialog/article-edit-dialog.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NgxAuthUtilsModule} from "ngx-auth-utils";
import { ArticleDetailsComponent } from './article-details/article-details.component';

@NgModule({
  declarations: [MainArticlesComponent, ArticleSummaryComponent, ArticleFormComponent, ArticleEditDialogComponent, ArticleDetailsComponent],
    imports: [
        CommonModule,
        BlogRoutingModule,
        StoreModule.forFeature('articles', articleReducer),
        EffectsModule.forFeature([ArticlesEffects]),
        SharedModule,
        RippleModule,
        DynamicDialogModule,
        ReactiveFormsModule,
        NgxAuthUtilsModule
    ],
})
export class BlogModule {}
