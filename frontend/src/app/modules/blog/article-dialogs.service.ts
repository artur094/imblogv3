import {Injectable} from '@angular/core';
import {DialogService} from "primeng/dynamicdialog";
import {Observable} from "rxjs";
import {ArticleDto} from "@core/http-gen/models/article-dto";
import {ArticleEditDialogComponent} from "@blog/article-edit-dialog/article-edit-dialog.component";

export interface ArticleDialogConfig {
  title: string;
  confirmButton: string;
  cancelButton: string;
}


@Injectable()
export class ArticleDialogsService {

  constructor(private dialogService: DialogService) { }

  openArticleForm(config: ArticleDialogConfig, article?: ArticleDto): Observable<ArticleDto> {
    return this.dialogService.open(
      ArticleEditDialogComponent, {
        showHeader: false,
        dismissableMask: true,
        width: 'auto',
        height: 'auto',
        styleClass: 'mobile-full-screen',
        data: {
          article: article,
          config: config
        }
      }
    ).onClose;
  }
}
