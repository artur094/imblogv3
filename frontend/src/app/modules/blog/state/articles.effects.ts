import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {ArticleResourceService} from "@core/http-gen/services/article-resource.service";
import {
  createArticle,
  createArticleSuccess, deleteArticle, deleteArticleSuccess, editArticle, editArticleSuccess,
  loadArticles,
  loadArticlesSuccess,
  loadCurrentArticle, loadCurrentArticleSuccess
} from "@blog/state/articles.actions";
import {exhaustMap, switchMap} from "rxjs";
import {map} from "rxjs/operators";

@Injectable()
export class ArticlesEffects {
  constructor(private actions$: Actions, private articleService: ArticleResourceService) {}

  loadArticles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadArticles),
      switchMap(() =>
        this.articleService.getAllPublishedArticlesUsingGet().pipe(
          map(articles => loadArticlesSuccess({articles}))))
    )
  })

  loadCurrentArticle$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadCurrentArticle),
      switchMap((action) =>
        this.articleService.getArticleUsingGet({id: action.articleId}).pipe(
          map(article => loadCurrentArticleSuccess({article}))))
    )
  })

  editArticle$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(editArticle),
      switchMap((action) =>
        this.articleService.updateArticleUsingPut({id: action.article.id as number, body: action.article}).pipe(
          map(article => editArticleSuccess({article})))) //TODO: should update also the list
    )
  })

  deleteArticle$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteArticle),
      switchMap((action) =>
        this.articleService.deleteArticleUsingDelete({id: action.article.id as number}).pipe(
          map(() => deleteArticleSuccess({articleId: action.article.id as number}))))  //TODO: should update also the list
    )
  })

  createArticle$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(createArticle),
      exhaustMap((action) =>
        this.articleService.createArticleUsingPost({body: action.article})
      ),
      exhaustMap(() => this.articleService.getAllPublishedArticlesUsingGet().pipe(
          map(articles => createArticleSuccess({articles}))))
    )
  })
}
