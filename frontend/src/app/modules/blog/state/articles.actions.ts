import {createAction, props} from "@ngrx/store";
import {ArticleDto} from "@core/http-gen/models";

export const loadArticles = createAction('[Article List] Load Articles');
export const loadArticlesSuccess = createAction('[Article API] Load Success', props<{articles: ArticleDto[]}>());
export const loadArticlesFailure = createAction('[Article API] Load Fail', props<{error: string}>());

export const createArticle = createAction('[Article List] Create Article', props<{article: ArticleDto}>());
export const createArticleSuccess = createAction('[Article API] Create Article Success', props<{articles: ArticleDto[]}>());
export const createArticleFailure = createAction('[Article API] Create Article Fail', props<{error: string}>());

export const editArticle = createAction('[Article Detail] Edit Article', props<{article: ArticleDto}>());
export const editArticleSuccess = createAction('[Article API] Edit Article Success', props<{article: ArticleDto}>());
export const editArticleFailure = createAction('[Article API] Edit Article Fail', props<{error: string}>());

export const deleteArticle = createAction('[Article Detail] Delete Article', props<{article: ArticleDto}>());
export const deleteArticleSuccess = createAction('[Article API] Delete Article Success', props<{articleId: number}>());
export const deleteArticleFailure = createAction('[Article API] Delete Article Fail', props<{error: string}>());

export const loadCurrentArticle = createAction('[Article Detail] Load Current Article', props<{articleId: number}>());
export const loadCurrentArticleSuccess = createAction('[Article API] Load Current Article Success', props<{article: ArticleDto}>());
export const loadCurrentArticleFailure = createAction('[Article API] Load Current Article Failure', props<{error: string}>());
export const clearCurrentArticle = createAction('[Article List] Clea Current Article');
