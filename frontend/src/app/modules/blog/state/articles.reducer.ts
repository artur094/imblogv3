import {createFeatureSelector, createReducer, createSelector, on} from "@ngrx/store";
import {ArticleDto} from "@core/http-gen/models/article-dto";
import * as AppState from "@state/app.state"
import {
  clearCurrentArticle,
  createArticleSuccess, deleteArticleSuccess, editArticleSuccess,
  loadArticles,
  loadArticlesSuccess,
  loadCurrentArticle, loadCurrentArticleSuccess
} from "@blog/state/articles.actions";

export interface State extends AppState.State{
  products: ArticleState
}

export interface ArticleState {
  articles: ArticleDto[],
  currentArticle?: ArticleDto
}

const initialState: ArticleState = {
  articles: [],
  currentArticle: undefined
}

// Do not export this as we access using our custom selectors
const getArticleFeatureState = createFeatureSelector<ArticleState>('articles');

export const getCurrentArticle = createSelector(
  getArticleFeatureState,
  state => state.currentArticle
);

export const getArticles = createSelector(
  getArticleFeatureState,
  state => state.articles
);

export const articleReducer = createReducer<ArticleState>(
   initialState,
  on(createArticleSuccess, (state, action) => {
    return {
      ...state,
      articles: action.articles
    };
  }),
  on(loadArticlesSuccess, (state, action) => {
    return {
      ...state,
      articles: action.articles
    };
  }),
  on(loadCurrentArticleSuccess, (state, action) => {
    return {
      ...state,
      currentArticle: action.article
    };
  }),
  on(editArticleSuccess, (state, action) => {
    const newCurrentArticle = state.currentArticle?.id === action.article.id ? action.article : Object.assign({}, state.currentArticle)
    const newArticles = state.articles.map(storedArticle => storedArticle.id === action.article.id ? action.article : storedArticle);

    return {
      ...state,
      currentArticle: newCurrentArticle,
      articles: newArticles
    };
  }),
  on(deleteArticleSuccess, (state, action) => {
    const newCurrentArticle = state.currentArticle?.id === action.articleId ? undefined : Object.assign({}, state.currentArticle)
    const newArticles = state.articles.filter(article => article.id !== action.articleId);

    return {
      ...state,
      currentArticle: newCurrentArticle,
      articles: newArticles
    };
  }),
  on(clearCurrentArticle, (state) => {
    return {
      ...state,
      currentArticle: undefined
    };
  })
);
