import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {ARTICLE_STATUS} from "@core/enums";

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent {
  options = [{
    label: 'Draft',
    value: ARTICLE_STATUS.DRAFT
  },
    {
      label: 'Published',
      value: ARTICLE_STATUS.PUBLISHED
    }]

  @Input() form!: FormGroup;
}
