import {Component, Input} from '@angular/core';
import {ArticleDto} from "@core/http-gen/models/article-dto";

@Component({
  selector: 'app-article-summary',
  templateUrl: './article-summary.component.html',
  styleUrls: ['./article-summary.component.scss']
})
export class ArticleSummaryComponent {
  @Input() article!: ArticleDto
}
