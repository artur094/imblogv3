import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainArticlesComponent} from './main-articles/main-articles.component';
import {ArticleDetailsComponent} from "@blog/article-details/article-details.component";

const routes: Routes = [
  {
    path: '',
    component: MainArticlesComponent,
    pathMatch: 'full',
  },
  {
    path: ':id',
    component: ArticleDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogRoutingModule {}
