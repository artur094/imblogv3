import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {getArticles} from "@blog/state/articles.reducer";
import {createArticle, loadArticles} from "@blog/state/articles.actions";
import {ArticleDialogsService} from "@blog/article-dialogs.service";
import {DialogService} from "primeng/dynamicdialog";
import {first} from "rxjs";
import {USER_ROLES} from "@core/enums";
import {ArticleDto} from "@core/http-gen/models/article-dto";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-main-articles',
  templateUrl: './main-articles.component.html',
  styleUrls: ['./main-articles.component.scss'],
  providers: [DialogService, ArticleDialogsService]
})
export class MainArticlesComponent implements OnInit {
  readonly USER_ROLES = USER_ROLES;
  readonly articles$ = this.store.select(getArticles);

  constructor(private store: Store, private articleDialogs: ArticleDialogsService, private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.store.dispatch(loadArticles());
  }

  openNewArticleDialog(): void {
    this.articleDialogs.openArticleForm({
      title: 'Create Article',
      confirmButton: 'Create',
      cancelButton: 'Cancel'
    },).pipe(first()).subscribe((article) => {
      if(article) {
        this.store.dispatch(createArticle({article}))
      }
    })
  }

  openArticle(article: ArticleDto): void {
    this.router.navigate([article.id], {relativeTo: this.activatedRoute})
  }
}
