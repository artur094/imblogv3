import {Component} from '@angular/core';
import {Store} from "@ngrx/store";
import {getCurrentArticle} from "@blog/state/articles.reducer";
import {ActivatedRoute, Router} from "@angular/router";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {createArticle, deleteArticle, editArticle, loadCurrentArticle} from "@blog/state/articles.actions";
import {filter, map} from "rxjs/operators";
import {USER_ROLES} from "@core/enums";
import {first} from "rxjs";
import {ArticleDialogsService} from "@blog/article-dialogs.service";
import {ArticleDto} from "@core/http-gen/models/article-dto";
import {ConfirmDialogService} from "@shared/dialogs/confirm-dialog/confirm-dialog.service";
import {DialogService} from "primeng/dynamicdialog";

@UntilDestroy()
@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.scss'],
  providers: [ArticleDialogsService, ConfirmDialogService, DialogService]
})
export class ArticleDetailsComponent {
  readonly USER_ROLES = USER_ROLES;
  readonly article$ = this.store.select(getCurrentArticle);

  constructor(private store: Store, private activatedRoute: ActivatedRoute, private articleDialogs: ArticleDialogsService, private confirmDialogService: ConfirmDialogService, private router: Router) {
    this.activatedRoute.paramMap.pipe(untilDestroyed(this), filter(data => data.has('id') && data.get('id') != null), map(data => +(data.get('id') as string))).subscribe((id) => this.store.dispatch(loadCurrentArticle({ articleId: id})))
  }

  editArticle(article: ArticleDto): void {
    this.articleDialogs.openArticleForm(
      {
        title: 'Edit Article',
        confirmButton: 'Edit',
        cancelButton: 'Cancel'
      },
      article
    ).pipe(first()).subscribe((updatedData) => {
      const updatedArticle: ArticleDto = {...article, ...updatedData}

      if(updatedArticle) {
        this.store.dispatch(editArticle({article: updatedArticle}))
      }
    })
  }

  deleteArticle(article: ArticleDto): void {
    this.confirmDialogService.open({
      title: 'You are deleting an article',
      text: 'This operation is not reversible.'
    }).pipe(first()).subscribe(res => {
      if(res) {
        this.store.dispatch(deleteArticle({article}))
        this.router.navigate(['..'], {relativeTo: this.activatedRoute})
      }
    })
  }
}
