import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";
import {ARTICLE_STATUS} from "@core/enums";
import {ArticleDialogConfig} from "@blog/article-dialogs.service";

@Component({
  selector: 'app-article-edit-dialog',
  templateUrl: './article-edit-dialog.component.html',
  styleUrls: ['./article-edit-dialog.component.scss']
})
export class ArticleEditDialogComponent implements OnInit {

  articleForm = this.fb.group({
    title: this.fb.control(undefined, Validators.required),
    body: this.fb.control(undefined, Validators.required),
    status: this.fb.control(ARTICLE_STATUS.DRAFT, Validators.required)
  })

  data!: ArticleDialogConfig;

  constructor(private fb: FormBuilder, private ref: DynamicDialogRef, private config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.data = this.config.data.config;
    this.articleForm.patchValue(this.config.data.article);
  }

  closeDialog(): void {
    this.ref.close();
  }

  confirmArticle(): void {
    if(this.articleForm.valid) {
      this.ref.close(this.articleForm.value);
    }
  }
}
