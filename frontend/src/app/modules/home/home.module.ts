import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {HomeRoutingModule} from '@home/home-routing.module';
import {NgxAuthUtilsModule} from 'ngx-auth-utils';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    HomeRoutingModule,
    CommonModule,
    NgxAuthUtilsModule,
  ],
})
export class HomeModule {}
