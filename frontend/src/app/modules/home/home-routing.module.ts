import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '@home/home/home.component';
import { NavigationBarComponent } from '@core/components/navigation-bar/navigation-bar.component';
import { FooterComponent } from '@core/components/footer/footer.component';
import { LoginComponent } from '../user/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
