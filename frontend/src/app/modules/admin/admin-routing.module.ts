import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '@admin/home/home.component';
import { NavigationBarComponent } from '@core/components/navigation-bar/navigation-bar.component';
import { FooterComponent } from '@core/components/footer/footer.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    outlet: 'navigation',
    component: NavigationBarComponent,
  },
  {
    path: '',
    outlet: 'footer',
    component: FooterComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
