import {Component} from '@angular/core';
import {NgxAuthService} from 'ngx-auth-utils';
import {PrimeNGConfig} from "primeng/api";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'frontend';

  constructor(private ngxAuthService: NgxAuthService, private primengConfig: PrimeNGConfig) {
    this.ngxAuthService.initialize().subscribe();
    this.primengConfig.ripple = true;
  }
}
