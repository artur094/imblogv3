import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NavigationBarComponent} from "@core/components/navigation-bar/navigation-bar.component";

const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () =>
      import('@admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'user',
    loadChildren: () => import('@user/user.module').then((m) => m.UserModule),
  },
  {
    path: 'blog',
    loadChildren: () => import('@blog/blog.module').then((m) => m.BlogModule),
  },
  {
    path: 'projects',
    loadChildren: () =>
      import('@projects/projects.module').then((m) => m.ProjectsModule),
  },
  {
    path: '',
    loadChildren: () => import('@home/home.module').then((m) => m.HomeModule),
  },
  {
    path: '',
    outlet: 'navigation',
    component: NavigationBarComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
