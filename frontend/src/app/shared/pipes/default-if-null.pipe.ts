import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultIfNull'
})
export class DefaultIfNullPipe implements PipeTransform {

  transform(value: unknown | undefined | null, defaultValue: unknown = ''): unknown {
    return value == null ? defaultValue : value;
  }
}
