import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";
import {ConfirmDialogConfig} from "@shared/dialogs/confirm-dialog/confirm-dialog.service";

@Component({
  selector: 'app-article-edit-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  data?: ConfirmDialogConfig;

  constructor(private ref: DynamicDialogRef, private config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.data = this.config.data.config;
  }

  closeDialog(): void {
    this.ref.close();
  }

  confirm(): void {
    this.ref.close(true);
  }
}
