import {Injectable} from '@angular/core';
import {DialogService} from "primeng/dynamicdialog";
import {Observable} from "rxjs";
import {ArticleDto} from "@core/http-gen/models/article-dto";
import {ArticleEditDialogComponent} from "@blog/article-edit-dialog/article-edit-dialog.component";
import {ConfirmDialogComponent} from "@shared/dialogs/confirm-dialog/confirm-dialog.component";

export interface ConfirmDialogConfig {
  title?: string;
  text?: string;
  confirmButton?: string;
  cancelButton?: string;
}

@Injectable()
export class ConfirmDialogService {

  constructor(private dialogService: DialogService) { }

  open(config?: ConfirmDialogConfig): Observable<boolean> {
    return this.dialogService.open(
      ConfirmDialogComponent, {
        showHeader: false,
        dismissableMask: true,
        width: 'auto',
        height: '100%',
        styleClass: 'mobile',
        data: {
          config
        }
      }
    ).onClose;
  }
}
