import {NgModule} from '@angular/core';
import {DefaultIfNullPipe} from "@shared/pipes/default-if-null.pipe";
import {ConfirmDialogComponent} from "@shared/dialogs/confirm-dialog/confirm-dialog.component";
import {RippleModule} from "primeng/ripple";

@NgModule({
  declarations: [DefaultIfNullPipe, ConfirmDialogComponent],
  imports: [
    RippleModule
  ],
  exports: [DefaultIfNullPipe]
})
export class SharedModule {}
