package it.morandizzi.imblog.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.morandizzi.imblog.IntegrationTest;
import it.morandizzi.imblog.config.FactoryBuilder;
import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.domain.Tag;
import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.domain.UserArticleReaction;
import it.morandizzi.imblog.domain.enums.Reaction;
import it.morandizzi.imblog.domain.enums.Status;
import it.morandizzi.imblog.repository.ArticleRepository;
import it.morandizzi.imblog.repository.TagRepository;
import it.morandizzi.imblog.repository.UserRepository;
import it.morandizzi.imblog.security.AuthoritiesConstants;
import it.morandizzi.imblog.service.ArticleService;
import it.morandizzi.imblog.service.dto.ArticleDTO;
import it.morandizzi.imblog.service.dto.TagDTO;
import it.morandizzi.imblog.service.dto.UserArticleReactionDTO;
import it.morandizzi.imblog.service.mapper.ArticleMapper;
import it.morandizzi.imblog.service.mapper.TagMapper;
import it.morandizzi.imblog.service.mapper.UserArticleReactionMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ArticleResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Transactional
public class ArticleResourceIT {

    private static final String TAG_NAME = "AAAAAAAAAA";
    private static final String ANOTHER_TAG_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.DRAFT;
    private static final Status UPDATED_STATUS = Status.PUBLISHED;

    private static final Instant DEFAULT_CREATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/articles";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_TAG_API_URL = ENTITY_API_URL_ID + "/tags";
    private static final String ENTITY_TAG_API_URL_NAME = ENTITY_TAG_API_URL + "/{tagName}";
    private static final String ENTITY_REACTION_API_URL = ENTITY_API_URL_ID + "/reactions";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private UserRepository userRepository;

    @Mock
    private ArticleRepository articleRepositoryMock;

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private UserArticleReactionMapper userArticleReactionMapper;

    @Autowired
    private TagMapper tagMapper;

    @Mock
    private ArticleService articleServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restArticleMockMvc;

    @Autowired
    private FactoryBuilder factoryBuilder;

    private Article draftArticle;
    private Article publishedArticle;
    private Article deletedArticle;
    private Tag tag;
    private Tag anotherTag;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Article createArticle(Status status) {
        Article article = new Article();
        article.setTitle(DEFAULT_TITLE);
        article.setBody(DEFAULT_BODY);
        article.setCreated(DEFAULT_CREATED);
        article.setStatus(status);
        return article;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Article createUpdatedArticle(Status status) {
        Article article = new Article();
        article.setTitle(UPDATED_TITLE);
        article.setBody(UPDATED_BODY);
        article.setCreated(UPDATED_CREATED);
        article.setStatus(status);
        return article;
    }

    @BeforeEach
    public void initTest() {
        publishedArticle = createArticle(Status.PUBLISHED);
        draftArticle = createArticle(Status.DRAFT);
        deletedArticle = createArticle(Status.DELETED);

        tag = TagResourceIT.createTag(TAG_NAME);
        anotherTag = TagResourceIT.createTag(ANOTHER_TAG_NAME);
    }

    @Test
    @Transactional
    void createArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.EDITOR));
        assertThat(articleRepository.findAll()).hasSize(0);

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);
        restArticleMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isCreated());

        em.flush();

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(1);
        Article testArticle = articleList.get(0);
        assertThat(testArticle.getTitle()).isEqualTo(articleDTO.getTitle());
        assertThat(testArticle.getBody()).isEqualTo(articleDTO.getBody());
        assertThat(testArticle.getCreated()).isEqualTo(articleDTO.getCreated());
        assertThat(testArticle.getStatus()).isEqualTo(articleDTO.getStatus());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = { AuthoritiesConstants.EDITOR })
    void createArticleWithExistingId() throws Exception {
        // Create the Article with an existing ID
        publishedArticle.setId(1L);
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        int databaseSizeBeforeCreate = articleRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restArticleMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = { AuthoritiesConstants.EDITOR })
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = articleRepository.findAll().size();
        // set the field null
        publishedArticle.setTitle(null);

        // Create the Article, which fails.
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        restArticleMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isBadRequest());

        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = { AuthoritiesConstants.USER })
    void getAllArticles() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(publishedArticle);
        articleRepository.saveAndFlush(draftArticle);
        articleRepository.saveAndFlush(deletedArticle);

        // Get all the articleList
        restArticleMockMvc
            .perform(get(ENTITY_API_URL + "/published?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(publishedArticle.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.[0].body").value(DEFAULT_BODY.toString()))
            .andExpect(jsonPath("$.[0].created").value(DEFAULT_CREATED.toString()))
            .andExpect(jsonPath("$.[0].status").value(Status.PUBLISHED.toString()));
    }

    private static Stream<Arguments> getRolesAndNumberOfArticles() {
        return Stream.of(
            Arguments.of("/published", AuthoritiesConstants.USER, false, status().isOk(), 1),
            Arguments.of("/published", AuthoritiesConstants.EDITOR, false, status().isOk(), 1),
            Arguments.of("/published", AuthoritiesConstants.EDITOR, true, status().isOk(), 1),
            Arguments.of("/published", AuthoritiesConstants.ADMIN, false, status().isOk(), 1),
            Arguments.of("/draft", AuthoritiesConstants.USER, false, status().isForbidden(), 0),
            Arguments.of("/draft", AuthoritiesConstants.EDITOR, false, status().isOk(), 0),
            Arguments.of("/draft", AuthoritiesConstants.EDITOR, true, status().isOk(), 1),
            Arguments.of("/draft", AuthoritiesConstants.ADMIN, false, status().isOk(), 1),
            Arguments.of("/deleted", AuthoritiesConstants.USER, false, status().isForbidden(), 0),
            Arguments.of("/deleted", AuthoritiesConstants.EDITOR, false, status().isForbidden(), 0),
            Arguments.of("/deleted", AuthoritiesConstants.EDITOR, true, status().isForbidden(), 0),
            Arguments.of("/deleted", AuthoritiesConstants.ADMIN, false, status().isOk(), 1)
        );
    }

    @ParameterizedTest
    @MethodSource("getRolesAndNumberOfArticles")
    @Transactional
    void getAllArticlesByRoles(String urlSuffix, String role, Boolean owner, ResultMatcher apiResult, Integer numberOfArticles)
        throws Exception {
        User user = factoryBuilder.createAndSaveUser(role, Set.of(role));

        // Initialize the database
        if (owner) {
            publishedArticle.setOwner(user);
            draftArticle.setOwner(user);
            deletedArticle.setOwner(user);
        }

        articleRepository.saveAndFlush(publishedArticle);
        articleRepository.saveAndFlush(draftArticle);
        articleRepository.saveAndFlush(deletedArticle);

        // Get all the articleList
        ResultActions response = restArticleMockMvc
            .perform(get(ENTITY_API_URL + urlSuffix + "?sort=id,desc").with(user(user)))
            .andExpect(apiResult);

        if (apiResult.equals(status().isOk())) {
            response
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(numberOfArticles)));
        }
    }

    @Test
    @Transactional
    void getArticle() throws Exception {
        // Initialize the database
        articleRepository.saveAndFlush(publishedArticle);

        // Get the article
        restArticleMockMvc
            .perform(get(ENTITY_API_URL_ID, publishedArticle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(publishedArticle.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.body").value(DEFAULT_BODY.toString()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.toString()));
    }

    @Test
    @Transactional
    void getNonExistingArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        restArticleMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE).with(user(user))).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.EDITOR));
        articleRepository.saveAndFlush(publishedArticle);

        user.addOwnedArticle(publishedArticle);
        em.flush();

        assertThat(articleRepository.findAll()).hasSize(1);

        em.detach(publishedArticle);
        publishedArticle.setTitle(UPDATED_TITLE);
        publishedArticle.setBody(UPDATED_BODY);
        publishedArticle.setCreated(UPDATED_CREATED);
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        restArticleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, articleDTO.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isOk());

        assertThat(articleRepository.findAll()).hasSize(1);

        em.flush();

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(1);
        Article testArticle = articleList.get(0);
        assertThat(testArticle.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testArticle.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testArticle.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    void putNonExistingArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, articleDTO.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamArticle() throws Exception {
        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(articleDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateArticleWithPatch() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        // Initialize the database
        publishedArticle.setOwner(user);
        articleRepository.saveAndFlush(publishedArticle);

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();

        // Update the article using partial update
        Article partialUpdatedArticle = new Article();
        partialUpdatedArticle.setId(publishedArticle.getId());

        partialUpdatedArticle.setBody(UPDATED_BODY);
        partialUpdatedArticle.setCreated(UPDATED_CREATED);

        restArticleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedArticle.getId())
                    .with(user(user))
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedArticle))
            )
            .andExpect(status().isOk());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
        Article testArticle = articleList.get(articleList.size() - 1);
        assertThat(testArticle.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testArticle.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testArticle.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    void fullUpdateArticleWithPatch() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        // Initialize the database
        publishedArticle.setOwner(user);
        articleRepository.saveAndFlush(publishedArticle);

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();

        // Update the article using partial update
        Article partialUpdatedArticle = new Article();
        partialUpdatedArticle.setId(publishedArticle.getId());

        partialUpdatedArticle.setTitle(UPDATED_TITLE);
        partialUpdatedArticle.setBody(UPDATED_BODY);
        partialUpdatedArticle.setCreated(UPDATED_CREATED);

        restArticleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedArticle.getId())
                    .with(user(user))
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedArticle))
            )
            .andExpect(status().isOk());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
        Article testArticle = articleList.get(articleList.size() - 1);
        assertThat(testArticle.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testArticle.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testArticle.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    void patchNonExistingArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, articleDTO.getId())
                    .with(user(user))
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(user(user))
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamArticle() throws Exception {
        int databaseSizeBeforeUpdate = articleRepository.findAll().size();
        publishedArticle.setId(count.incrementAndGet());

        // Create the Article
        ArticleDTO articleDTO = articleMapper.toDto(publishedArticle);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restArticleMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(articleDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Article in the database
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        // Initialize the database
        publishedArticle.setOwner(user);
        articleRepository.saveAndFlush(publishedArticle);

        int databaseSizeBeforeDelete = articleRepository.findAll().size();

        // Delete the article
        restArticleMockMvc
            .perform(delete(ENTITY_API_URL_ID, publishedArticle.getId()).with(user(user)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Article> articleList = articleRepository.findAll();
        assertThat(articleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void addNewTagToArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        publishedArticle.setOwner(user);
        articleRepository.saveAndFlush(publishedArticle);

        tagRepository.saveAndFlush(tag);
        TagDTO tagDTO = tagMapper.toDto(tag);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(0);

        restArticleMockMvc
            .perform(
                post(ENTITY_TAG_API_URL, publishedArticle.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tagDTO))
            )
            .andExpect(status().isNoContent());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(1);
    }

    @Test
    @Transactional
    void addExistingTagToArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        tagRepository.saveAndFlush(tag);
        TagDTO tagDTO = tagMapper.toDto(tag);

        publishedArticle.setOwner(user);
        publishedArticle.addTag(tag);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(1);

        restArticleMockMvc
            .perform(
                post(ENTITY_TAG_API_URL, publishedArticle.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tagDTO))
            )
            .andExpect(status().isBadRequest());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(1);
    }

    @Test
    @Transactional
    void addNonExistingTagToArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        TagDTO tagDTO = tagMapper.toDto(tag);

        publishedArticle.setOwner(user);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(0);

        restArticleMockMvc
            .perform(
                post(ENTITY_TAG_API_URL, publishedArticle.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tagDTO))
            )
            .andExpect(status().isNotFound());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(0);
    }

    @Test
    @Transactional
    void getArticleTags() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.ADMIN));

        tagRepository.saveAndFlush(tag);

        publishedArticle.setOwner(user);
        publishedArticle.addTag(tag);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(1);

        restArticleMockMvc
            .perform(get(ENTITY_TAG_API_URL, publishedArticle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].name").value(tag.getName()));
    }

    @Test
    @Transactional
    void removeTagFromArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("username", Set.of(AuthoritiesConstants.EDITOR));

        tagRepository.saveAndFlush(tag);

        publishedArticle.setOwner(user);
        publishedArticle.addTag(tag);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(1);

        restArticleMockMvc
            .perform(delete(ENTITY_TAG_API_URL_NAME, publishedArticle.getId(), tag.getName()).with(user(user)))
            .andExpect(status().isNoContent());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(0);
    }

    @Test
    @Transactional
    void addNewReactionToArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("userWithoutReactions", Set.of(AuthoritiesConstants.USER));
        articleRepository.saveAndFlush(publishedArticle);

        UserArticleReactionDTO userArticleReactionDTO = new UserArticleReactionDTO();
        userArticleReactionDTO.setReaction(Reaction.LIKE);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(0);

        em.clear();

        restArticleMockMvc
            .perform(
                post(ENTITY_REACTION_API_URL, publishedArticle.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userArticleReactionDTO))
            )
            .andExpect(status().isNoContent());

        em.flush();
        em.clear();

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(1);

        UserArticleReaction reaction = dbArticle.getReactions().iterator().next();
        assertThat(reaction.getReaction()).isEqualTo(Reaction.LIKE);
    }

    @Test
    @Transactional
    void updateReactionToArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("userWithReactions", Set.of(AuthoritiesConstants.USER));

        //UserArticleReaction userArticleReaction = new UserArticleReaction();
        //userArticleReaction.setUser(user);
        //userArticleReaction.setArticle(publishedArticle);
        //userArticleReaction.setReaction(Reaction.LIKE);

        publishedArticle.addReaction(user, Reaction.LIKE);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(1);

        UserArticleReaction reaction = dbArticle.getReactions().iterator().next();
        assertThat(reaction.getReaction()).isEqualTo(Reaction.LIKE);
        UserArticleReactionDTO userArticleReactionDTO = userArticleReactionMapper.toDto(reaction).reaction(Reaction.DISLIKE);

        restArticleMockMvc
            .perform(
                post(ENTITY_REACTION_API_URL, publishedArticle.getId())
                    .with(user(user))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userArticleReactionDTO))
            )
            .andExpect(status().isNoContent());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(1);
        reaction = dbArticle.getReactions().iterator().next();
        reaction.setReaction(Reaction.DISLIKE);
        assertThat(reaction.getReaction()).isEqualTo(Reaction.DISLIKE);
    }

    @Test
    @Transactional
    void removeReactionFromArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("userRemoveReaction", Set.of(AuthoritiesConstants.USER));
        publishedArticle.addReaction(user, Reaction.LIKE);
        articleRepository.save(publishedArticle);

        em.flush();

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(1);

        restArticleMockMvc
            .perform(delete(ENTITY_REACTION_API_URL, publishedArticle.getId()).with(user(user)))
            .andExpect(status().isNoContent());

        dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(0);
    }

    @Test
    @Transactional
    void getReactionsFromArticle() throws Exception {
        User user = factoryBuilder.createAndSaveUser("userRetrieveAllReactions", Set.of(AuthoritiesConstants.USER));

        //UserArticleReaction userArticleReaction = new UserArticleReaction();
        //userArticleReaction.setUser(user);
        //userArticleReaction.setArticle(publishedArticle);
        //userArticleReaction.setReaction(Reaction.LIKE);

        publishedArticle.addReaction(user, Reaction.LIKE);
        articleRepository.saveAndFlush(publishedArticle);

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(1);

        restArticleMockMvc
            .perform(get(ENTITY_REACTION_API_URL, publishedArticle.getId()).with(user(user)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].reaction").value(Reaction.LIKE.toString()));
    }

    @Test
    @Transactional
    void getOwnReactionsToArticle() throws Exception {
        //        articleRepository.saveAndFlush(publishedArticle);
        //
        //        User user = factoryBuilder.createAndSaveUser("userRetrieveOwnReaction", Set.of(AuthoritiesConstants.USER));
        //        User user2 = factoryBuilder.createAndSaveUser("userRetrieveOwnReaction2", Set.of(AuthoritiesConstants.USER));
        //
        //        UserArticleReaction reaction1 = new UserArticleReaction(user, publishedArticle, Reaction.LIKE);
        //        UserArticleReaction reaction2 = new UserArticleReaction(user2, publishedArticle, Reaction.DISLIKE);
        //        userArticleReactionRepository.saveAll(List.of(reaction1, reaction2));
        //        articleRepository.saveAndFlush(publishedArticle);
        //
        //        em.clear();
        //
        //        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        //        assertThat(dbArticle.getReactions()).hasSize(2);
        //
        //        restArticleMockMvc
        //            .perform(
        //                get(ENTITY_REACTION_API_URL + "/owner", publishedArticle.getId()).with(user(user))
        //            )
        //            .andExpect(status().isOk())
        //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        //            .andExpect(jsonPath("$.reaction").value(Reaction.LIKE.toString()));

    }
}
