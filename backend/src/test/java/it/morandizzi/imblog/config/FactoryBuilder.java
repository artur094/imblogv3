package it.morandizzi.imblog.config;

import it.morandizzi.imblog.domain.Authority;
import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.repository.AuthorityRepository;
import it.morandizzi.imblog.repository.UserRepository;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class FactoryBuilder {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User createAndSaveUser(String username, Set<String> roles) {
        User user = this.createUser(username, roles);
        return userRepository.saveAndFlush(user);
    }

    public User createUser(String username, Set<String> roles) {
        User user = new User();
        user.setLogin(username);
        user.setEmail(username + "@mail.to");
        user.setPassword(passwordEncoder.encode("password"));
        user.setAuthorities(roles.stream().map(this::createAndSaveAuthority).collect(Collectors.toSet()));
        return user;
    }

    public Authority createAndSaveAuthority(String role) {
        Authority authority = new Authority();
        authority.setName(role);
        return authorityRepository.saveAndFlush(authority);
    }
}
