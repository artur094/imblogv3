package it.morandizzi.imblog.repository;

import static org.assertj.core.api.Assertions.assertThat;

import it.morandizzi.imblog.IntegrationTest;
import it.morandizzi.imblog.config.FactoryBuilder;
import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.domain.Tag;
import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.domain.UserArticleReaction;
import it.morandizzi.imblog.domain.enums.Reaction;
import it.morandizzi.imblog.domain.enums.Status;
import it.morandizzi.imblog.security.AuthoritiesConstants;
import it.morandizzi.imblog.web.rest.ArticleResourceIT;
import it.morandizzi.imblog.web.rest.TagResourceIT;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
class ArticleRepositoryIT {

    @Autowired
    FactoryBuilder factoryBuilder;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    UserRepository userRepository;

    @Test
    @Transactional
    void testOneToMany() {
        Article article1 = ArticleResourceIT.createArticle(Status.PUBLISHED);
        Article article2 = ArticleResourceIT.createArticle(Status.PUBLISHED);
        articleRepository.saveAll(List.of(article1, article2));
        articleRepository.flush();

        User user = factoryBuilder.createUser("user", Set.of(AuthoritiesConstants.USER));
        user.addOwnedArticle(article1);
        user.addOwnedArticle(article2);
        userRepository.saveAndFlush(user);

        entityManager.clear();

        User dbUser = userRepository.findOneByLogin(user.getLogin()).orElseThrow();
        assertThat(dbUser.getOwnedArticles()).hasSize(2);

        Article dbArticle1 = articleRepository.getOne(article1.getId());
        assertThat(dbArticle1.getOwner()).isNotNull();
        Article dbArticle2 = articleRepository.getOne(article2.getId());
        assertThat(dbArticle2.getOwner()).isNotNull();
    }

    @Test
    @Transactional
    void testManyToOne() {
        User user = factoryBuilder.createAndSaveUser("user", Set.of(AuthoritiesConstants.USER));

        Article article1 = ArticleResourceIT.createArticle(Status.PUBLISHED);
        article1.setOwner(user);
        Article article2 = ArticleResourceIT.createArticle(Status.PUBLISHED);
        article2.setOwner(user);
        articleRepository.saveAll(List.of(article1, article2));
        articleRepository.flush();

        entityManager.clear();

        User dbUser = userRepository.findOneByLogin(user.getLogin()).orElseThrow();
        assertThat(dbUser.getOwnedArticles()).hasSize(2);

        Article dbArticle1 = articleRepository.getOne(article1.getId());
        assertThat(dbArticle1.getOwner()).isNotNull();
        Article dbArticle2 = articleRepository.getOne(article2.getId());
        assertThat(dbArticle2.getOwner()).isNotNull();
    }

    @Test
    @Transactional
    void testManyToMany() {
        Tag tag1 = TagResourceIT.createTag("tag1");
        Tag tag2 = TagResourceIT.createTag("tag2");
        tagRepository.saveAll(List.of(tag1, tag2));
        tagRepository.flush();

        Article publishedArticle = ArticleResourceIT.createArticle(Status.PUBLISHED);
        publishedArticle.addTag(tag1);
        publishedArticle.addTag(tag2);
        articleRepository.saveAndFlush(publishedArticle);

        entityManager.clear();

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getTags()).hasSize(2);

        Tag dbTag1 = tagRepository.getOne(tag1.getId());
        assertThat(dbTag1.getArticles()).hasSize(1);
        Tag dbTag2 = tagRepository.getOne(tag2.getId());
        assertThat(dbTag2.getArticles()).hasSize(1);
    }

    @Test
    @Transactional
    void testManyToManyWithCompositeKeyInverse() {
        User user = factoryBuilder.createAndSaveUser("user", Set.of(AuthoritiesConstants.USER));
        User user2 = factoryBuilder.createAndSaveUser("user1", Set.of(AuthoritiesConstants.USER));

        Article publishedArticle = ArticleResourceIT.createArticle(Status.PUBLISHED);
        publishedArticle.addReaction(user, Reaction.LIKE);
        publishedArticle.addReaction(user2, Reaction.DISLIKE);
        articleRepository.saveAndFlush(publishedArticle);

        //entityManager.flush();
        entityManager.clear();

        Article dbArticle = articleRepository.findById(publishedArticle.getId()).orElseThrow();
        assertThat(dbArticle.getReactions()).hasSize(2);

        User dbUser1 = userRepository.getOne(user.getId());
        assertThat(dbUser1.getReactions()).hasSize(1);
        User dbUser2 = userRepository.getOne(user2.getId());
        assertThat(dbUser2.getReactions()).hasSize(1);
    }
}
