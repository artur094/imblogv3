package it.morandizzi.imblog.service.mapper;

import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.service.dto.ArticleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { UserMapper.class, TagMapper.class })
public interface ArticleMapper extends EntityMapper<ArticleDTO, Article> {
    ArticleDTO toDto(Article s);

    @Mapping(target = "owner", ignore = true)
    @Mapping(target = "tags", ignore = true)
    Article toEntity(ArticleDTO articleDTO);
}
