package it.morandizzi.imblog.service.mapper;

import it.morandizzi.imblog.domain.UserArticleReaction;
import it.morandizzi.imblog.service.dto.UserArticleReactionDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface UserArticleReactionMapper extends EntityMapper<UserArticleReactionDTO, UserArticleReaction> {
    UserArticleReactionDTO toDto(UserArticleReaction s);
}
