package it.morandizzi.imblog.service;

import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.domain.Tag;
import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.domain.enums.Status;
import it.morandizzi.imblog.repository.ArticleRepository;
import it.morandizzi.imblog.repository.TagRepository;
import it.morandizzi.imblog.service.dto.ArticleDTO;
import it.morandizzi.imblog.service.dto.TagDTO;
import it.morandizzi.imblog.service.dto.UserArticleReactionDTO;
import it.morandizzi.imblog.service.errors.TagAlreadyAssociated;
import it.morandizzi.imblog.service.mapper.ArticleMapper;
import it.morandizzi.imblog.service.mapper.TagMapper;
import it.morandizzi.imblog.service.mapper.UserArticleReactionMapper;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Article}.
 */
@Service
@Transactional
public class ArticleService {

    private final String ENTITY_NAME = "Article";

    private final Logger log = LoggerFactory.getLogger(ArticleService.class);

    private final ArticleRepository articleRepository;
    private final TagRepository tagRepository;

    private final ArticleMapper articleMapper;
    private final TagMapper tagMapper;
    private final UserArticleReactionMapper userArticleReactionMapper;

    private final UserService userService;

    private final EntityManager entityManager;

    public ArticleService(
        ArticleRepository articleRepository,
        TagRepository tagRepository,
        ArticleMapper articleMapper,
        TagMapper tagMapper,
        UserArticleReactionMapper userArticleReactionMapper,
        UserService userService,
        EntityManager entityManager
    ) {
        this.articleRepository = articleRepository;
        this.tagRepository = tagRepository;
        this.articleMapper = articleMapper;
        this.tagMapper = tagMapper;
        this.userArticleReactionMapper = userArticleReactionMapper;
        this.userService = userService;
        this.entityManager = entityManager;
    }

    public ArticleDTO save(ArticleDTO articleDTO) {
        log.debug("Request to save Article : {}", articleDTO);

        Article article = articleMapper.toEntity(articleDTO);

        if (article.getId() == null) {
            User user = userService.getUserWithAuthorities().orElseThrow();
            article.setOwner(user);
        }
        article = articleRepository.save(article);
        return articleMapper.toDto(article);
    }

    public Optional<ArticleDTO> partialUpdate(ArticleDTO articleDTO) {
        log.debug("Request to partially update Article : {}", articleDTO);

        return articleRepository
            .findById(articleDTO.getId())
            .map(existingArticle -> {
                articleMapper.partialUpdate(existingArticle, articleDTO);
                return existingArticle;
            })
            .map(articleMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ArticleDTO> findAllByStatus(Pageable pageable, Status status) {
        log.debug("Request to get all Articles with status {}", status);

        return articleRepository.findAll(articleRepository.hasStatus(status), pageable).map(articleMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ArticleDTO> findAllOwnedByStatus(Pageable pageable, Status status) {
        log.debug("Request to get all owned Articles with status {}", status);

        User user = userService.getUserWithAuthorities().orElseThrow();

        return articleRepository
            .findAll(articleRepository.hasStatus(status).and(articleRepository.isArticleOwner(user)), pageable)
            .map(articleMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<ArticleDTO> findOne(Long id) {
        log.debug("Request to get Article : {}", id);
        return articleRepository.findById(id).map(articleMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete Article : {}", id);
        articleRepository.deleteById(id);
    }

    public void addTag(Long articleId, String tagName) throws NoSuchElementException, TagAlreadyAssociated {
        Article article = articleRepository.findById(articleId).orElseThrow();
        Tag tag = tagRepository.findByName(tagName).orElseThrow();

        if (article.getTags().contains(tag)) {
            throw new TagAlreadyAssociated();
        }

        article.addTag(tag);
    }

    public void removeTag(Long articleId, String tagName) {
        Article article = articleRepository.findById(articleId).orElseThrow();
        Tag tag = tagRepository.findByName(tagName).orElseThrow();

        article.removeTag(tag);
    }

    public List<TagDTO> getTags(Long articleId) {
        Article article = articleRepository.findById(articleId).orElseThrow();
        return article.getTags().stream().map(tagMapper::toDto).collect(Collectors.toList());
    }

    public void addReaction(Long articleId, UserArticleReactionDTO reactionDTO) {
        Article article = articleRepository.findById(articleId).orElseThrow();
        User user = userService.getUserWithAuthorities().orElseThrow();
        article.addReaction(user, reactionDTO.getReaction());
    }

    public void removeReaction(Long articleId) {
        Article article = articleRepository.findById(articleId).orElseThrow();
        User user = userService.getUserWithAuthorities().orElseThrow();
        article.removeReaction(user);
    }

    public UserArticleReactionDTO getReaction(Long articleId) {
        Article article = articleRepository.findById(articleId).orElseThrow();
        User user = userService.getUserWithAuthorities().orElseThrow();

        return article
            .getReactions()
            .stream()
            .filter(articleReaction -> user.equals(articleReaction.getUser()))
            .findFirst()
            .map(userArticleReactionMapper::toDto)
            .orElseGet(UserArticleReactionDTO::new);
    }

    public List<UserArticleReactionDTO> getReactions(Long articleId) {
        return articleRepository
            .findById(articleId)
            .map(Article::getReactions)
            .stream()
            .flatMap(Collection::stream)
            .map(userArticleReactionMapper::toDto)
            .collect(Collectors.toList());
    }
}
