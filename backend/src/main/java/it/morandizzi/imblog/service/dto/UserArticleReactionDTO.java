package it.morandizzi.imblog.service.dto;

import it.morandizzi.imblog.domain.enums.Reaction;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link it.morandizzi.imblog.domain.Tag} entity.
 */
public class UserArticleReactionDTO implements Serializable {

    @NotNull
    private Reaction reaction;

    public Reaction getReaction() {
        return reaction;
    }

    public void setReaction(Reaction reaction) {
        this.reaction = reaction;
    }

    public UserArticleReactionDTO reaction(Reaction reaction) {
        this.reaction = reaction;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserArticleReactionDTO)) {
            return false;
        }

        UserArticleReactionDTO userArticleReactionDTO = (UserArticleReactionDTO) o;
        if (this.reaction == null) {
            return false;
        }
        return Objects.equals(this.reaction, userArticleReactionDTO.reaction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.reaction);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserArticleReactionDTO{" +
            "reaction='" + getReaction() + "'" +
            "}";
    }
}
