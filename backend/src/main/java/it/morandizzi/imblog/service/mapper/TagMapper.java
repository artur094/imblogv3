package it.morandizzi.imblog.service.mapper;

import it.morandizzi.imblog.domain.Tag;
import it.morandizzi.imblog.service.dto.TagDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = { UserMapper.class, TagMapper.class })
public interface TagMapper extends EntityMapper<TagDTO, Tag> {
    TagDTO toDto(Tag s);
    Tag toEntity(TagDTO articleDTO);
}
