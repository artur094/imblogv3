package it.morandizzi.imblog.service.errors;

public class TagAlreadyAssociated extends Exception {

    private static final long serialVersionUID = 1L;

    public TagAlreadyAssociated() {
        super("tag-already-associated");
    }
}
