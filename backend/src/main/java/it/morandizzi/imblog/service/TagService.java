package it.morandizzi.imblog.service;

import it.morandizzi.imblog.domain.Tag;
import it.morandizzi.imblog.repository.TagRepository;
import it.morandizzi.imblog.service.dto.TagDTO;
import it.morandizzi.imblog.service.mapper.TagMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TagService {

    private final Logger log = LoggerFactory.getLogger(TagService.class);

    private final TagRepository tagRepository;

    private final TagMapper tagMapper;

    public TagService(TagRepository tagRepository, TagMapper tagMapper) {
        this.tagRepository = tagRepository;
        this.tagMapper = tagMapper;
    }

    public Tag create(String name) {
        log.debug("Request to save Tag : {}", name);
        Tag tag = new Tag();
        tag.setName(name);
        return tagRepository.saveAndFlush(tag);
    }

    @Transactional(readOnly = true)
    public List<TagDTO> findAll() {
        log.debug("Request to get all Tags");
        return tagRepository.findAll().stream().map(tagMapper::toDto).collect(Collectors.toList());
    }

    public void delete(String name) {
        log.debug("Request to delete Tag : {}", name);
        tagRepository.deleteByName(name);
    }

    public boolean exists(String name) {
        return tagRepository.existsByName(name);
    }
}
