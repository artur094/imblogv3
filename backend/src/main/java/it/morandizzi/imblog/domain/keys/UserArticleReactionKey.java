package it.morandizzi.imblog.domain.keys;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserArticleReactionKey implements Serializable {

    @Column(name = "user_id")
    Long userId;

    @Column(name = "article_id")
    Long articleId;

    public UserArticleReactionKey() {}

    public UserArticleReactionKey(Long userId, Long articleId) {
        this.userId = userId;
        this.articleId = articleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserArticleReactionKey that = (UserArticleReactionKey) o;
        return Objects.equals(userId, that.userId) && Objects.equals(articleId, that.articleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, articleId);
    }

    @Override
    public String toString() {
        return "UserArticleReactionKey{" + "userId=" + userId + ", articleId=" + articleId + '}';
    }
}
