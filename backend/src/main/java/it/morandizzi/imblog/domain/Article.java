package it.morandizzi.imblog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import it.morandizzi.imblog.domain.enums.Reaction;
import it.morandizzi.imblog.domain.enums.Status;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * A Article.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "body", nullable = false)
    private String body;

    @NotNull
    @CreatedDate
    @Column(name = "created", nullable = false)
    private Instant created;

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne()
    private User owner;

    @ManyToMany
    @JoinTable(name = "rel_article__tag", joinColumns = @JoinColumn(name = "article_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @JsonIgnoreProperties(value = { "articles" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserArticleReaction> reactions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Instant getCreated() {
        return this.created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public User getOwner() {
        return this.owner;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void addTag(Tag tag) {
        this.tags.add(tag);
        tag.getArticles().add(this);
    }

    public void removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getArticles().remove(this);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<UserArticleReaction> getReactions() {
        return reactions;
    }

    public void addReaction(User user, Reaction reaction) {
        UserArticleReaction userArticleReaction = new UserArticleReaction(user, this, reaction);
        this.reactions.add(userArticleReaction);
        user.getReactions().add(userArticleReaction);
    }

    public void removeReaction(User user) {
        this.reactions.stream()
            .filter(reaction -> reaction.getUser().equals(user))
            .findAny()
            .ifPresent(reaction -> {
                this.reactions.remove(reaction);
                user.getReactions().remove(reaction);
                reaction.setUser(null);
                reaction.setArticle(null);
            });
    }

    public void clearReactions() {
        this.reactions.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Article)) {
            return false;
        }
        return id != null && id.equals(((Article) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Article{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
