package it.morandizzi.imblog.domain;

import it.morandizzi.imblog.domain.enums.Reaction;
import it.morandizzi.imblog.domain.keys.UserArticleReactionKey;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * A User Reaction to an Article.
 */
@Entity
public class UserArticleReaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    UserArticleReactionKey id = new UserArticleReactionKey();

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @MapsId("articleId")
    @JoinColumn(name = "article_id")
    Article article;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "reaction", nullable = false)
    private Reaction reaction;

    public UserArticleReaction() {}

    public UserArticleReaction(User user, Article article, Reaction reaction) {
        this.id = new UserArticleReactionKey(user.getId(), article.getId());
        this.user = user;
        this.article = article;
        this.reaction = reaction;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UserArticleReactionKey getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (user == null) {
            this.id.setUserId(null);
        }
        this.user = user;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        if (user == null) {
            this.id.setArticleId(null);
        }
        this.article = article;
    }

    public Reaction getReaction() {
        return reaction;
    }

    public void setReaction(Reaction reaction) {
        this.reaction = reaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserArticleReaction)) {
            return false;
        }
        return id != null && id.equals(((UserArticleReaction) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "ArticleReaction{" + "id=" + id + ", user=" + user + ", article=" + article + ", reaction=" + reaction + '}';
    }
}
