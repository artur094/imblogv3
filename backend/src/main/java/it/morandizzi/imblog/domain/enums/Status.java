package it.morandizzi.imblog.domain.enums;

public enum Status {
    DRAFT,
    DELETED,
    PUBLISHED,
}
