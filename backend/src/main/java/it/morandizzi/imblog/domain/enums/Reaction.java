package it.morandizzi.imblog.domain.enums;

public enum Reaction {
    LIKE,
    LOVE,
    DISLIKE,
}
