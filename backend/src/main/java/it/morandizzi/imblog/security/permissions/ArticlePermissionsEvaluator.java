package it.morandizzi.imblog.security.permissions;

import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.domain.enums.Status;
import it.morandizzi.imblog.repository.ArticleRepository;
import it.morandizzi.imblog.security.AuthoritiesConstants;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service("articlePermissionEvaluator")
public class ArticlePermissionsEvaluator extends BasePermissionsEvaluator {

    protected final List<String> AUTHORITIES_CAN_VIEW_DRAFT = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_ADD = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_UPDATE = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_DELETE = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_ADD_TAG = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_DELETE_TAG = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_ADD_REACTION = List.of(AuthoritiesConstants.USER);
    protected final List<String> AUTHORITIES_CAN_DELETE_REACTION = List.of(AuthoritiesConstants.USER);

    protected static final List<Status> NON_DELETED_STATUSES = List.of(Status.DRAFT, Status.PUBLISHED);

    private ArticleRepository articleRepository;

    public ArticlePermissionsEvaluator(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public boolean canAdd(Authentication authentication) {
        return hasPermissions(authentication, AUTHORITIES_CAN_ADD);
    }

    public boolean canUpdate(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    hasPermissions(authentication, AUTHORITIES_CAN_UPDATE) &&
                    this.isOwnerOfArticle(authentication, article) &&
                    NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    public boolean canViewOne(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    Status.PUBLISHED.equals(article.getStatus()) &&
                    this.canViewPublished(authentication) ||
                    Status.DRAFT.equals(article.getStatus()) &&
                    this.canViewDraft(authentication) &&
                    this.isOwnerOfArticle(authentication, article)
                )
                .orElse(false)
        );
    }

    public boolean canViewPublished(Authentication authentication) {
        return true;
    }

    public boolean canViewDraft(Authentication authentication) {
        return this.hasPermissions(authentication, AUTHORITIES_CAN_VIEW_DRAFT);
    }

    public boolean canViewDeleted(Authentication authentication) {
        return this.isAdmin(authentication);
    }

    public boolean canDelete(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    hasPermissions(authentication, AUTHORITIES_CAN_DELETE) &&
                    this.isOwnerOfArticle(authentication, article) &&
                    NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    public boolean canAddTag(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    this.hasPermissions(authentication, AUTHORITIES_CAN_ADD_TAG) &&
                    this.isOwnerOfArticle(authentication, article) &&
                    NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    public boolean canDeleteTag(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    this.hasPermissions(authentication, AUTHORITIES_CAN_DELETE_TAG) &&
                    this.isOwnerOfArticle(authentication, article) &&
                    NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    public boolean canViewTag(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId).map(article -> NON_DELETED_STATUSES.contains(article.getStatus())).orElse(false)
        );
    }

    public boolean canViewReaction(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId).map(article -> NON_DELETED_STATUSES.contains(article.getStatus())).orElse(false)
        );
    }

    public boolean canAddReaction(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    this.hasPermissions(authentication, AUTHORITIES_CAN_ADD_REACTION) && NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    public boolean canDeleteReaction(Authentication authentication, Long articleId) {
        return (
            this.isAdmin(authentication) ||
            this.articleRepository.findById(articleId)
                .map(article ->
                    this.hasPermissions(authentication, AUTHORITIES_CAN_DELETE_REACTION) &&
                    NON_DELETED_STATUSES.contains(article.getStatus())
                )
                .orElse(false)
        );
    }

    private boolean isOwnerOfArticle(Authentication authentication, Article article) {
        return article.getOwner() != null && article.getOwner().equals(authentication.getPrincipal());
    }
}
