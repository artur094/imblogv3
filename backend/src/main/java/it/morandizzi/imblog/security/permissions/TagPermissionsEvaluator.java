package it.morandizzi.imblog.security.permissions;

import it.morandizzi.imblog.security.AuthoritiesConstants;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service("tagPermissionEvaluator")
public class TagPermissionsEvaluator extends BasePermissionsEvaluator {

    protected final List<String> AUTHORITIES_CAN_ADD = List.of(AuthoritiesConstants.EDITOR);
    protected final List<String> AUTHORITIES_CAN_DELETE = List.of();

    public boolean canAdd(Authentication authentication) {
        return hasPermissions(authentication, AUTHORITIES_CAN_ADD);
    }

    public boolean canView(Authentication authentication) {
        return true;
    }

    public boolean canDelete(Authentication authentication) {
        return hasPermissions(authentication, AUTHORITIES_CAN_DELETE);
    }
}
