package it.morandizzi.imblog.security.permissions;

import it.morandizzi.imblog.security.AuthoritiesConstants;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public abstract class BasePermissionsEvaluator {

    protected boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(AuthoritiesConstants.ADMIN::equals);
    }

    protected boolean hasPermissions(Authentication authentication, List<String> requiredAuthorities) {
        List<String> authorities = new ArrayList<>(requiredAuthorities);
        authorities.add(AuthoritiesConstants.ADMIN);
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(authorities::contains);
    }
}
