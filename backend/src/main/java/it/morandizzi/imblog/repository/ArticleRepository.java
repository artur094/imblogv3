package it.morandizzi.imblog.repository;

import com.querydsl.core.types.dsl.BooleanExpression;
import it.morandizzi.imblog.domain.Article;
import it.morandizzi.imblog.domain.QArticle;
import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.domain.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Article entity.
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long>, QuerydslPredicateExecutor<Article> {
    default BooleanExpression isArticleOwner(User user) {
        return QArticle.article.owner.eq(user);
    }

    default BooleanExpression hasStatus(Status status) {
        return QArticle.article.status.eq(status);
    }
}
