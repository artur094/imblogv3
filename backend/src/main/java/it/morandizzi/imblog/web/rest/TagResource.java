package it.morandizzi.imblog.web.rest;

import it.morandizzi.imblog.service.TagService;
import it.morandizzi.imblog.service.dto.TagDTO;
import it.morandizzi.imblog.web.rest.errors.BadRequestAlertException;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping(value ="/api", produces = "application/json")
public class TagResource {

    private final Logger log = LoggerFactory.getLogger(TagResource.class);

    private static final String ENTITY_NAME = "tag";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TagService tagService;

    public TagResource(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * {@code POST  /tags} : Create a new tag.
     *
     * @param tag the tag to create.
     * @return the {@link ResponseEntity} with status {@code 204} and with body the new tag, or with status {@code 400 (Bad Request)} if the tag has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tags")
    @PreAuthorize("@tagPermissionEvaluator.canAdd(authentication)")
    public ResponseEntity<Void> createTag(@Valid @RequestBody TagDTO tag) throws URISyntaxException {
        log.debug("REST request to save Tag : {}", tag.getName());
        if (tagService.exists(tag.getName())) {
            throw new BadRequestAlertException("Tag with given name already exists", ENTITY_NAME, "tagexists");
        }
        tagService.create(tag.getName());
        return ResponseEntity.noContent().build();
    }

    /**
     * {@code GET  /tags} : get all the tags' names.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tags' names in body.
     */
    @GetMapping("/tags")
    @PreAuthorize("@tagPermissionEvaluator.canView(authentication)")
    public List<TagDTO> getAllTags() {
        log.debug("REST request to get all Tags' names");
        return tagService.findAll();
    }

    /**
     * {@code DELETE  /tags/:name} : delete the "name" tag.
     *
     * @param name the name of the tag to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tags/{name}")
    @PreAuthorize("@tagPermissionEvaluator.canDelete(authentication)")
    public ResponseEntity<Void> deleteTag(@PathVariable String name) {
        log.debug("REST request to delete Tag : {}", name);
        tagService.delete(name);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, name.toString()))
            .build();
    }
}
