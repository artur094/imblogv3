/**
 * View Models used by Spring MVC REST controllers.
 */
package it.morandizzi.imblog.web.rest.vm;
