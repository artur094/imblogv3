package it.morandizzi.imblog.web.rest;

import it.morandizzi.imblog.domain.User;
import it.morandizzi.imblog.domain.enums.Status;
import it.morandizzi.imblog.repository.ArticleRepository;
import it.morandizzi.imblog.service.ArticleService;
import it.morandizzi.imblog.service.UserService;
import it.morandizzi.imblog.service.dto.ArticleDTO;
import it.morandizzi.imblog.service.dto.TagDTO;
import it.morandizzi.imblog.service.dto.UserArticleReactionDTO;
import it.morandizzi.imblog.service.errors.TagAlreadyAssociated;
import it.morandizzi.imblog.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping(value ="/api", produces = "application/json")
public class ArticleResource {

    private final Logger log = LoggerFactory.getLogger(ArticleResource.class);

    private static final String ENTITY_NAME = "article";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;

    private final ArticleService articleService;

    private final ArticleRepository articleRepository;

    public ArticleResource(UserService userService, ArticleService articleService, ArticleRepository articleRepository) {
        this.userService = userService;
        this.articleService = articleService;
        this.articleRepository = articleRepository;
    }

    /**
     * {@code POST  /articles} : Create a new article.
     *
     * @param articleDTO the articleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new articleDTO, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/articles")
    @PreAuthorize("@articlePermissionEvaluator.canAdd(authentication)")
    public ResponseEntity<ArticleDTO> createArticle(@Valid @RequestBody ArticleDTO articleDTO) throws URISyntaxException {
        log.debug("REST request to save Article : {}", articleDTO);
        if (articleDTO.getId() != null) {
            throw new BadRequestAlertException("A new article cannot already have an ID", ENTITY_NAME, "idexists");
        }

        ArticleDTO result = articleService.save(articleDTO);
        return ResponseEntity
            .created(new URI("/api/articles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /articles/:id} : Updates an existing article.
     *
     * @param id the id of the articleDTO to save.
     * @param articleDTO the articleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated articleDTO,
     * or with status {@code 400 (Bad Request)} if the articleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the articleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/articles/{id}")
    @PreAuthorize("@articlePermissionEvaluator.canUpdate(authentication, #id)")
    public ResponseEntity<ArticleDTO> updateArticle(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ArticleDTO articleDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Article : {}, {}", id, articleDTO);
        if (articleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, articleDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!articleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ArticleDTO> result = articleService.partialUpdate(articleDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, articleDTO.getId().toString())
        );
    }

    /**
     * {@code PATCH  /articles/:id} : Partial updates given fields of an existing article, field will ignore if it is null
     *
     * @param id the id of the articleDTO to save.
     * @param articleDTO the articleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated articleDTO,
     * or with status {@code 400 (Bad Request)} if the articleDTO is not valid,
     * or with status {@code 404 (Not Found)} if the articleDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the articleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/articles/{id}", consumes = "application/merge-patch+json")
    @PreAuthorize("@articlePermissionEvaluator.canUpdate(authentication, #id)")
    public ResponseEntity<ArticleDTO> partialUpdateArticle(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ArticleDTO articleDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Article partially : {}, {}", id, articleDTO);
        if (articleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, articleDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!articleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ArticleDTO> result = articleService.partialUpdate(articleDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, articleDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /articles} : get all published articles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of articles in body.
     */
    @GetMapping("/articles/published")
    @PreAuthorize("@articlePermissionEvaluator.canViewPublished(authentication)")
    public ResponseEntity<List<ArticleDTO>> getAllPublishedArticles(Pageable pageable) {
        log.debug("REST request to get a page of published Articles");
        Page<ArticleDTO> page = articleService.findAllByStatus(pageable, Status.PUBLISHED);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /articles/draft} : get all owned draft articles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of articles in body.
     */
    @GetMapping("/articles/draft")
    @PreAuthorize("@articlePermissionEvaluator.canViewDraft(authentication)")
    public ResponseEntity<List<ArticleDTO>> getAllDraftArticles(Pageable pageable) {
        log.debug("REST request to get a page of draft Articles");

        User user = userService.getUserWithAuthorities().orElseThrow();

        Page<ArticleDTO> page;
        if (user.isAdmin()) {
            page = articleService.findAllByStatus(pageable, Status.DRAFT);
        } else {
            page = articleService.findAllOwnedByStatus(pageable, Status.DRAFT);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /articles/deleted} : get all deleted articles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of articles in body.
     */
    @GetMapping("/articles/deleted")
    @PreAuthorize("@articlePermissionEvaluator.canViewDeleted(authentication)")
    public ResponseEntity<List<ArticleDTO>> getAllDeletedArticles(Pageable pageable) {
        log.debug("REST request to get a page of deleted Articles");

        Page<ArticleDTO> page = articleService.findAllByStatus(pageable, Status.DELETED);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /articles/:id} : get the "id" article.
     *
     * @param id the id of the articleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the articleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/articles/{id}")
    @PreAuthorize("@articlePermissionEvaluator.canViewOne(authentication, #id)")
    public ResponseEntity<ArticleDTO> getArticle(@PathVariable Long id) {
        log.debug("REST request to get Article : {}", id);
        Optional<ArticleDTO> articleDTO = articleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(articleDTO);
    }

    /**
     * {@code DELETE  /articles/:id} : delete the "id" article.
     *
     * @param id the id of the articleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/articles/{id}")
    @PreAuthorize("@articlePermissionEvaluator.canDelete(authentication, #id)")
    public ResponseEntity<Object> deleteArticle(@PathVariable Long id) {
        log.debug("REST request to delete Article : {}", id);
        articleService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code POST  /article/:id/tags} : Add a tag to article.
     *
     * @param id the id of the article to add the tag.
     * @param tag the tag to add to the article.
     * @return the {@link ResponseEntity} with status {@code 204 (No Content)}, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/articles/{id}/tags")
    @PreAuthorize("@articlePermissionEvaluator.canAddTag(authentication, #id)")
    public ResponseEntity<Object> addTagToArticle(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TagDTO tag
    ) throws URISyntaxException, NoSuchElementException {
        log.debug("REST request to add tag {} to Article : {}", tag, id);

        try {
            articleService.addTag(id, tag.getName());
        } catch (TagAlreadyAssociated ex) {
            throw new BadRequestAlertException("Tag already associated to article", ENTITY_NAME, "tag-already-associated");
        }
        return ResponseEntity.noContent().build();
    }

    /**
     * {@code GET  /article/:id/tags} : Get all tags of article.
     *
     * @param id the id of the article to add the tag.
     * @return the {@link ResponseEntity} with status {@code 200} and the list of tags, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @GetMapping("/articles/{id}/tags")
    @PreAuthorize("@articlePermissionEvaluator.canViewTag(authentication, #id)")
    public List<TagDTO> getAllArticleTags(@PathVariable(value = "id", required = false) final Long id) throws URISyntaxException {
        log.debug("REST request to get all tags of Article : {}", id);

        return this.articleService.getTags(id);
    }

    /**
     * {@code DELETE  /article/:id/tags/:name} : Remove tag from article.
     *
     * @param id the id of the article to add the tag.
     * @param tagName the tag of the article to remove.
     * @return the {@link ResponseEntity} with status {@code 204}, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @DeleteMapping("/articles/{id}/tags/{tag}")
    @PreAuthorize("@articlePermissionEvaluator.canDeleteTag(authentication, #id)")
    public ResponseEntity<Object> removeTagFromArticle(
        @PathVariable(value = "id", required = false) final Long id,
        @PathVariable(value = "tag", required = false) final String tagName
    ) throws URISyntaxException {
        log.debug("REST request to delete tag {} from Article : {}", tagName, id);

        this.articleService.removeTag(id, tagName);
        return ResponseEntity.noContent().build();
    }

    /**
     * {@code POST  /article/:id/reactions} : Add or update reaction to article.
     *
     * @param id the id of the article to add the tag.
     * @param reactionDTO the reaction to the article.
     * @return the {@link ResponseEntity} with status {@code 204 (No Content)}, or with status {@code 400 (Bad Request)}.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/articles/{id}/reactions")
    @PreAuthorize("@articlePermissionEvaluator.canAddReaction(authentication, #id)")
    public ResponseEntity<Object> addOrUpdateReactionToArticle(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UserArticleReactionDTO reactionDTO
    ) throws URISyntaxException, NoSuchElementException {
        log.debug("REST request to update reaction {} to Article : {}", reactionDTO, id);

        articleService.addReaction(id, reactionDTO);
        return ResponseEntity.noContent().build();
    }

    /**
     * {@code GET  /article/:id/reactions/owner} : Get the user reaction to the article.
     *
     * @param id the id of the article.
     * @return the {@link ResponseEntity} with status {@code 200} and user reaction, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @GetMapping("/articles/{id}/reactions/owner")
    @PreAuthorize("@articlePermissionEvaluator.canViewReaction(authentication, #id)")
    public UserArticleReactionDTO getReaction(@PathVariable(value = "id", required = false) final Long id) throws URISyntaxException {
        log.debug("REST request to get user reaction to Article : {}", id);

        return this.articleService.getReaction(id);
    }

    /**
     * {@code GET  /article/:id/reactions} : Get all reactions to article.
     *
     * @param id the id of the article.
     * @return the {@link ResponseEntity} with status {@code 200} and the list of reactions, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @GetMapping("/articles/{id}/reactions")
    @PreAuthorize("@articlePermissionEvaluator.canViewReaction(authentication, #id)")
    public List<UserArticleReactionDTO> getAllReactionsOfArticle(@PathVariable(value = "id", required = false) final Long id)
        throws URISyntaxException {
        log.debug("REST request to get all reactions of Article : {}", id);

        return this.articleService.getReactions(id);
    }

    /**
     * {@code DELETE  /article/:id/reactions} : Remove the reaction from article.
     *
     * @param id the id of the article from where to remove the reaction.
     * @return the {@link ResponseEntity} with status {@code 204}, or with status {@code 400 (Bad Request)} if the article has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @DeleteMapping("/articles/{id}/reactions")
    @PreAuthorize("@articlePermissionEvaluator.canDeleteReaction(authentication, #id)")
    public ResponseEntity<Object> removeReaction(@PathVariable(value = "id", required = false) final Long id) throws URISyntaxException {
        log.debug("REST request to remove reaction from Article : {}", id);

        this.articleService.removeReaction(id);
        return ResponseEntity.noContent().build();
    }
}
